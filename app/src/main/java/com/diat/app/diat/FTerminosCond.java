package com.diat.app.diat;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.auxclases.TerminosCondiciones;

import static android.view.View.INVISIBLE;


public class FTerminosCond extends Fragment {

private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.diat_frag_terminos, container, false);
        //OCULTAR LA HAMBURGUER
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(INVISIBLE);

        displayTerminos();
        return view;
    }


    private void displayTerminos(){
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.light.ttf");
        Typeface typeBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.bold.ttf");

        TerminosCondiciones tc = new TerminosCondiciones();
        String terCondicTitle = tc.titulos[0];
        String terCondicText = tc.Terminos;


        TextView tvTitle;
        tvTitle = (TextView) view.findViewById(R.id.titleTerminos);
        tvTitle.setText(terCondicTitle);
        tvTitle.setTypeface(typeBold);


        TextView tvText;
        tvText = (TextView) view.findViewById(R.id.tvTerminos);
        tvText.setText(terCondicText);
        tvText.setTypeface(type);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).hideToolCenter();
        ((MainActivity) getActivity()).hidelayTerminos();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity) getActivity()).showToolCenter();
        ((MainActivity) getActivity()).showlayTerminos();
    }
}
