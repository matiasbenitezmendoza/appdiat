package com.diat.app.diat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.diat.app.navigation.LandingActivityB;
import com.diat.app.auxclases.NavListModel;
import com.diat.app.auxclases.Validaciones;
import com.diat.app.auxclases.firebase.FirebaseKeys;
import com.diat.app.navigation.FNuestrosProductos;
import com.diat.app.navigation.FTips;
import com.diat.app.registro.FRegistro;
import com.diat.app.registro.FRegistroRegimen;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.diat.app.webservices.Servicios;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private ScrimInsetsFrameLayout sifl;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private boolean doubleBackToExitPressedOnce = false;
    private FragmentManager manager;
    private CallbackManager callbackManager;
    public static int fbCase = -1;
    private String[] datosUser = new String[4];
    private Servicios servicios;
    private String tokenDispositivo;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        FacebookSdk.sdkInitialize(getApplicationContext());
        sharedPreferences = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        AppEventsLogger.activateApp(getApplication());
        manager = getSupportFragmentManager();
        servicios = new Servicios(this);
        facebook();
        resetShared();
        SetDrawer();
        inicio();
    }

    private void inicio() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FragmentInicio(), "FInicio")
                .commit();
        //Se resetea a 0 cuando inicia la app
        SharedPreferences userDetails = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        userDetails.edit().putString("isLoged", "0").apply();
    }

    private void fragment_tips(int logueado) {
        hidelayTerminos();
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FTips(), "FTips")
                .addToBackStack(null)
                .commit();
    }

    private void fragment_nuestrosProductos(int logueado) {
        hidelayTerminos();
        manager
                .beginTransaction()
                //.setCustomAnimations(R.anim.slid_in_up, R.anim.slide_out_up)
                .replace(R.id.flInicio, new FNuestrosProductos(), "FNuestrosProductos")
                .addToBackStack(null)
                .commit();
    }

    public void logeo(View view) {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FLogin(), "toFLogin")
                .addToBackStack(null)
                .commit();
    }

    public void registro(View view) {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FRegistro(), "FRegistro")
                .addToBackStack(null)
                .commit();
    }

    public void terminosyCond(View view) {
        manager
                .beginTransaction()
                //.setCustomAnimations(R.anim.slid_in_up, R.anim.slide_out_up)
                .replace(R.id.flInicio, new FTerminosCond())
                .addToBackStack(null)
                .commit();
    }

    public void avisoLegal(View view) {
        manager
                .beginTransaction()
                //.setCustomAnimations(R.anim.slid_in_up, R.anim.slide_out_up)
                .replace(R.id.flInicio, new FALegal())
                .addToBackStack(null)
                .commit();
    }

    public void avisoPrivacidad(View view) {
        manager
                .beginTransaction()
                //.setCustomAnimations(R.anim.slid_in_up, R.anim.slide_out_up)
                .replace(R.id.flInicio, new FAPrivacidad())
                .addToBackStack(null)
                .commit();
    }

    public void hidelayTerminos() {
        LinearLayout llTerm;
        llTerm = (LinearLayout) findViewById(R.id.llTerm);
        llTerm.setVisibility(GONE);
    }

    public void showlayTerminos() {
        LinearLayout llTerm;
        llTerm = (LinearLayout) findViewById(R.id.llTerm);
        llTerm.setVisibility(VISIBLE);
    }


    public void hideToolCenter() {
        ImageView toolC = (ImageView) findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(INVISIBLE);
        toolR.setVisibility(VISIBLE);
    }

    public void showToolCenter() {
        ImageView toolC = (ImageView) findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(VISIBLE);
        toolR.setVisibility(INVISIBLE);
    }

    public void SetDrawer() {
        sifl = (ScrimInsetsFrameLayout) findViewById(R.id.scrimInsentsFrameLayout);
        toolbar = (Toolbar) findViewById(R.id.include);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawLanding);
        ListView listView = (ListView) findViewById(R.id.navdrawerlist);
        NavAdapter adapter = new NavAdapter();
        if (listView != null) {
            listView.setAdapter(adapter);
        }

        setSupportActionBar(toolbar);
        drawerLayout.setStatusBarBackgroundColor(
                getResources().getColor(R.color.gris_toolbar));

        ImageView hamburgerMenu = (ImageView) toolbar.findViewById(R.id.hamburgerMenu);
        hamburgerMenu.setImageResource(R.drawable.menu_hamburger);
        hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawerLayout.isDrawerOpen(sifl)) {
                    drawerLayout.openDrawer(sifl);
                } else {
                    drawerLayout.closeDrawer(sifl);
                }
            }
        });
        toolbar.setBackgroundResource(R.drawable.layer_list_toolbar);

    }

    public class NavAdapter extends BaseAdapter {

        private ArrayList<NavListModel> lista;
        private LayoutInflater inflater = null;
        public Resources res;


        SharedPreferences varReferenceCargo = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        private String isLogedUser = varReferenceCargo.getString("isLoged", "");


        NavAdapter() {
            lista = new ArrayList<>();
            lista.add(new NavListModel(R.drawable.ico_tips, "TIPS"));
            lista.add(new NavListModel(R.drawable.ico_menuproducts, "NUESTROS PRODUCTOS"));


            inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint({"ViewHolder", "InflateParams"})
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi;
            NavAdapter.Holder holder = new NavAdapter.Holder();
            vi = inflater.inflate(R.layout.adapter_nav_item, null);
            holder.imgNav = (ImageView) vi.findViewById(R.id.imgNav);
            holder.tvNav = (TextView) vi.findViewById(R.id.tvNav);


            holder.imgNav.setImageResource(lista.get(position).getImagen());
            holder.tvNav.setText(lista.get(position).getNombre());

            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavClick(position);
                }
            });
            return vi;
        }

        class Holder {
            ImageView imgNav;
            TextView tvNav;
        }
    }

    public void NavClick(int position) {
        switch (position) {
            case 0:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                fragment_tips(0);
                break;
            case 1:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                fragment_nuestrosProductos(0);
                break;
        }

    }

    private void facebook() {
        servicios = new Servicios(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest graphRequest = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        List<String> datos = Validaciones.respuestaFacebook(null, object);
                                        switch (fbCase) {
                                            case 1:
                                                assert datos != null;
                                                tokenDispositivo = getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE).getString(FirebaseKeys.TOKEN, "");
                                                servicios.login(Arrays.asList(
                                                        datos.get(3),
                                                        datos.get(0),
                                                        tokenDispositivo),
                                                        new Servicios.DataCallback() {
                                                            @Override
                                                            public void onResult(boolean result) {
                                                                if (result) {
                                                                    startActivity(new Intent(getApplicationContext(), LandingActivityB.class));
                                                                    finish();
                                                                }
                                                            }
                                                        });
                                                break;


                                            case 2:
                                                assert datos != null;
                                                datosUser[0] = datos.get(1);//first name
                                                datosUser[1] = datos.get(2);//last name
                                                datosUser[2] = datos.get(3);//email
                                                datosUser[3] = datos.get(0); //pass
                                                FRegistroRegimen fragRegReg = new FRegistroRegimen();
                                                Bundle args = new Bundle();
                                                args.putStringArray("datosUsuario", datosUser);
                                                fragRegReg.setArguments(args);
                                                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                                ft.replace(R.id.flInicio, fragRegReg, "fRegRegimen");
                                                ft.commit();
                                                break;


                                        }
                                    }
                                }
                        );
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email,picture.type(large)");
                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }


                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(), "Proceso Cancelado", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(getApplicationContext(), "Error de Facebook", Toast.LENGTH_SHORT).show();
                        Log.e("Facebook", error.toString());
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetShared() {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        edit.putString("idcat", "");
        edit.putString("regimen", "");
        edit.putString("tipoPlan", "");
        edit.putString("token", "");
        edit.putString("referenciaCargo", "");
        edit.putString("flagFueRegistro", "0");
        edit.putString("isLoged", "");
        edit.putString("name", "");
        edit.putString("lastName", "");
        edit.putString("imagen", "");
        edit.putString("proxima_consulta", "");
        edit.putString("last_weigth", "");
        edit.putString("advance", "");
        edit.putString("last_weigth_days", "");
        edit.putString("weigth_actual", "");
        edit.putString("iddieta", "");
        edit.putString("peso_total_comunidad", "");
        edit.putString("activo", "");
        edit.putString("pesoDeseado", "");
        edit.putString("dias", "");
        edit.putString("banner", "");
        edit.putString("idCompania", "");
        edit.putString("cuestionario", "");
        edit.commit();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(sifl)) {
            drawerLayout.closeDrawer(sifl);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getText(R.string.atras_para_salir), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
        }
    }

}

/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LinearLayout llTerm;
        llTerm = (LinearLayout) findViewById(R.id.llTerm);
        llTerm.setVisibility(VISIBLE);

    }
*/