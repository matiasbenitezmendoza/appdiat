package com.diat.app.diat;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static android.view.View.VISIBLE;


public class  FragmentInicio extends Fragment {

    private View view;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.diat_frag_inicio, container, false);
        return view;
    }


    @Override
    public void onResume(){

        //hacer visible la hamburguesa
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(VISIBLE);


        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        llTerm.setVisibility(VISIBLE);


        super.onResume();
    }
}







































