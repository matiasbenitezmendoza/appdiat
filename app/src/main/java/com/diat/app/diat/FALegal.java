package com.diat.app.diat;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.auxclases.TerminosCondiciones;

import static android.view.View.INVISIBLE;


public class FALegal extends Fragment {
private View view;

    public FALegal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.diat_frag_legal, container, false);
        //OCULTAR LA HAMBURGUER
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(INVISIBLE);
        displayALegal();
        return view;

    }







    private void displayALegal(){
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.light.ttf");
        Typeface typeBold = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.bold.ttf");

        TerminosCondiciones al = new TerminosCondiciones();
        String teralTitle = al.titulos[1];
        String teralText = al.ALegal;


        TextView tvTitle;
        tvTitle = (TextView) view.findViewById(R.id.titleALegal);
        tvTitle.setText(teralTitle);
        tvTitle.setTypeface(typeBold);

        TextView tvText;
        tvText = (TextView) view.findViewById(R.id.tvALegal);
        tvText.setText(teralText);
        tvText.setTypeface(type);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).hideToolCenter();
        ((MainActivity) getActivity()).hidelayTerminos();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity) getActivity()).showToolCenter();
        ((MainActivity) getActivity()).showlayTerminos();
    }
}
