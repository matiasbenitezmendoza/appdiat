package com.diat.app.diat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.navigation.LandingActivityB;


public class SplashActivity extends AppCompatActivity {

    private final int DURACION_SPLASH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diat_content_splash);
        SharedPreferences sharedPreferences = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        final String isLogedUser = sharedPreferences.getString("isLoged", "");
        TextView texto = (TextView) findViewById(R.id.txDiat);
        TextView derechos = (TextView) findViewById(R.id.derechos);

        ImageView imgSplash = (ImageView) findViewById(R.id.imgSplash);

        Animation animacion = AnimationUtils.loadAnimation(this, R.anim.splash);
        Animation animImg = AnimationUtils.loadAnimation(this, R.anim.splash_img);
        texto.startAnimation(animacion);
        derechos.startAnimation(animacion);
        imgSplash.startAnimation(animImg);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                // Cuando pasen los 1 segundos, pasamos a la actividad principal de la aplicación
                if (isLogedUser.equals("1")){
                    startActivity(new Intent(getApplicationContext(), LandingActivityB.class));
                    finish();
                }
                else{
                    finish();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }
            }
        }, DURACION_SPLASH);


    }

}
