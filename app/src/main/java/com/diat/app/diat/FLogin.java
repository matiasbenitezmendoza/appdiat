package com.diat.app.diat;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.diat.app.navigation.LandingActivityB;
import com.diat.app.auxclases.Validaciones;
import com.diat.app.auxclases.firebase.FirebaseKeys;
import com.facebook.login.LoginManager;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CHARGE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CONFIG;
import static com.diat.app.auxclases.Constantes.KeysConfig.NOTIFICATIONS;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FLogin extends Fragment {
    private View view;
    private Switch swReg;
    private Button btnFaceb;
    private Button btnRegi;
    private EditText edtMail;
    private EditText edtPass;
    private String tokenDispositivo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.diat_frag_login, container, false);
        //OCULTAR LA HAMBURGUER
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(INVISIBLE);
        sharedConfiguration();


        /*-----------------CAMBIO DE ESTADO DEL SWITCH-------------------------------------------*/
        swReg = (Switch) view.findViewById(R.id.swtLogin);
        btnFaceb = (Button) view.findViewById(R.id.btnLoginFace);
        btnRegi = (Button) view.findViewById(R.id.buttonEntrar);
        btnFaceb.setAlpha(0.6f);
        btnRegi.setAlpha(0.6f);

        swReg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnFaceb.setEnabled(true);
                    btnRegi.setEnabled(true);
                    btnFaceb.setAlpha(1f);
                    btnRegi.setAlpha(1f);
                }
                else {
                    btnFaceb.setEnabled(false);
                    btnRegi.setEnabled(false);
                    btnFaceb.setAlpha(0.6f);
                    btnRegi.setAlpha(0.6f);
                }
            }
        });
        /*---------------------------------------------------------------------------------------*/



        //LLAMA LA METODO DE LOGUEO
        logIn();
        return view;
    }

    private void sharedConfiguration(){
        SharedPreferences preferencias = getContext().getSharedPreferences(CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString(NOTIFICATIONS,"0");
        editor.putString(CHARGE,"0");
        editor.commit();
    }



        public void logIn(){
            Button btnLogin, btnLoginFB;
            btnLogin = (Button) view.findViewById(R.id.buttonEntrar);
            btnLoginFB = (Button) view.findViewById(R.id.btnLoginFace);
            edtMail = (EditText) view.findViewById(R.id.edtCorreo);
            edtPass = (EditText) view.findViewById(R.id.edtContrasenia);

            tokenDispositivo = getContext().getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE).getString(FirebaseKeys.TOKEN, "");

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (edtMail.getText().toString().equals("") || edtPass.getText().toString().equals("")) {
                        Toast.makeText(getContext(), getString(R.string.campo_vacio), Toast.LENGTH_SHORT).show();
                    } else if (!Validaciones.isEmail(edtMail.getText().toString())) {
                        Toast.makeText(getContext(), getString(R.string.formato_incorrecto), Toast.LENGTH_SHORT).show();
                    } else {
                        new Servicios(getContext()).login(Arrays.asList(
                                edtMail.getText().toString(),
                                edtPass.getText().toString(),
                                tokenDispositivo),
                                new Servicios.DataCallback() {
                                    @Override
                                    public void onResult(boolean result) {
                                        if (result) {
                                            getActivity().finish();
                                            startActivity(new Intent(getApplicationContext(), LandingActivityB.class));
                                        }
                                    }
                                });
                    }
                }
            });


            btnLoginFB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //getActivity().getSharedPreferences(Constantes.UserKeys.shared_conf, Context.MODE_PRIVATE).edit().putString("tipo_login", "0").commit();
                    MainActivity.fbCase = 1;
                    LoginManager.getInstance().logOut();
                    LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));

                }
            });


        }







}
