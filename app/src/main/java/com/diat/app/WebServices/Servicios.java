package com.diat.app.webservices;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.diat.app.auxclases.Constantes;
import com.diat.app.auxclases.Constantes.UserKeys;
import com.diat.app.auxclases.Constantes.WebServices;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.auxclases.ModelChat;
import com.diat.app.auxclases.Utilidades;
import com.diat.app.auxclases.Validaciones;
import com.diat.app.diat.R;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

import static android.content.Context.MODE_PRIVATE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CHARGE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CONFIG;
import static com.diat.app.auxclases.Constantes.KeysConfig.IDCAT;
import static com.diat.app.auxclases.Constantes.KeysConfig.NOTIFICATIONS;
import static com.diat.app.auxclases.Constantes.PayKeys.idusuario;


public class Servicios {

    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private Context contexto;
    private JSONArray array;
    private String r;
    private String label;

    public Servicios(Context context) {
        contexto = context;
        label = 'Web Services';
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        alertDialog = new AlertDialog.Builder(context)
                .setMessage("Algo ha pasado...")
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                .create();
    }

    /**
     * interface para la captura de respuesta a la espera del servicio
     */
    public interface DataCallback {
        /**
         * @param result resultado metodo en espera
         */
        void onResult(boolean result);

        interface Extra {
            void onResult(boolean result, String extra);
        }

        interface ExtraValue {
            void onResult(boolean result, String extra, int caso);
        }

        interface ExtraAny {
            void onResult(boolean result, ModelChat modelChat);
        }
    }

    /**
     * @param mensaje Cuando en este parametro se le envie un valor nulo se usara el mensaje default
     */
    public void progressStat(String mensaje) {
        if (mensaje != null)
            progressDialog.setMessage(mensaje);
        if (!progressDialog.isShowing())
            progressDialog.show();
        else
            progressDialog.dismiss();
    }

    /**
     * @param mensaje Cuando en este parametro se le envie un valor nulo se usara el mensaje default
     */
    public void alertDialogStat(String mensaje) {
        if (mensaje != null)
            alertDialog.setMessage(mensaje);
        if (!alertDialog.isShowing())
            alertDialog.show();
        else
            alertDialog.dismiss();
    }

    /**
     * @param data Se recibe un arreglo de cadenas con los datos de login,
     *             campo 0 Correo
     *             campo 1 Contraseña
     */
    public void login(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        List<String> datosLogin = Validaciones.respuestaLogin("respuesta", response);
                        switch (Integer.parseInt(datosLogin.get(0))) {
                            case 0:
                                alertDialogStat("Usuario no registrado");
                                dataCallback.onResult(false);
                                break;
                            case 1:

                                SharedPreferences userDetails = contexto.getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                //SharedPreferences.Editor edit = userDetails.edit();
                                userDetails.edit().putString("isLoged", "1").apply();
                                userDetails.edit().putString("idcat", datosLogin.get(1)).apply();
                                userDetails.edit().putString("name", datosLogin.get(2)).apply();
                                userDetails.edit().putString("lastName", datosLogin.get(3)).apply();
                                userDetails.edit().putString("imagen", datosLogin.get(4)).apply();
                                userDetails.edit().putString("activo", datosLogin.get(7)).apply();
                                userDetails.edit().putString("tipoPlan", datosLogin.get(8)).apply();
                                userDetails.edit().putString("idCompania", datosLogin.get(9)).apply();
                                userDetails.edit().putString("cuestionario", datosLogin.get(10)).apply();
                                userDetails.edit().commit();

                                SharedPreferences userConfig = contexto.getSharedPreferences(CONFIG, MODE_PRIVATE);
                                //SharedPreferences.Editor editC = userConfig.edit();
                                userConfig.edit().putString(NOTIFICATIONS, datosLogin.get(5)).apply();
                                userConfig.edit().putString(CHARGE, datosLogin.get(6)).apply();
                                userConfig.edit().commit();

                                dataCallback.onResult(true);

                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constantes.LoginKeys.usuario, data.get(0));
                params.put(Constantes.LoginKeys.password, data.get(1));
                params.put(Constantes.LoginKeys.token, data.get(2));
                params.put("so", "1");
                return params;
            }
        };

        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest, WebServices.tag_login);

    }


    /**
     * OBTIENE LA INFORMACIÓN NECESARIA PARA LA PANTALLA DE INICIO, CUANDO UN USER ISLOGIN
     *
     * @param data
     * @param dataCallback
     * @return
     */
    public void getInfoInicio(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);

        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.info_inicio,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        List<String> datosStart = Validaciones.responseStart(null, response);
                        switch (Integer.parseInt(datosStart.get(0))) {
                            case 0:
                                alertDialogStat("Error al relizar la transacción");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                SharedPreferences userDetails = contexto.getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                SharedPreferences.Editor edit = userDetails.edit();
                                userDetails.edit().putString("iddieta", datosStart.get(1)).apply();
                                userDetails.edit().putString("banner", datosStart.get(2)).apply();
                                edit.commit();
                                GlobalVars.infoStart = response;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constantes.PayKeys.idusuario, data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * REGISTRO CON CORREO ELECTRONICO
     *
     * @param data
     * @param dataCallback
     */
    public void registro(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.registro,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        List<String> datos = Validaciones.respuestaRegistro(null, response);
                        switch (Integer.parseInt(datos.get(0))) {
                            case 2:
                                alertDialogStat("Usuario ya registrado");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                SharedPreferences userDetails = contexto.getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                SharedPreferences.Editor edit = userDetails.edit();
                                userDetails.edit().putString("idcat", datos.get(1).trim()).apply();
                                edit.commit();
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(UserKeys.nombre, data.get(0));
                params.put(UserKeys.apellido, data.get(1));
                params.put(UserKeys.email, data.get(2));
                params.put(UserKeys.passwd, data.get(3));
                params.put(UserKeys.regimen, data.get(4));
                params.put(UserKeys.plan, data.get(5));
                params.put(UserKeys.registro, data.get(6));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * ENVIA EL PESO DE LA PANTALLA REGISTRO DE PESO
     *
     * @param data         recibe el peso y idusuario
     * @param dataCallback
     */
    public void weigthRegister(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.registra_peso,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);

                        switch (Validaciones.respuestaSencilla("Registro peso", response)) {
                            case 0:
                                alertDialogStat("error al registrar su peso, intente nuevamente!");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constantes.RegisterWeighKeys.idusuario, data.get(0));
                params.put(Constantes.RegisterWeighKeys.peso, data.get(1));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * METODO QUE GUARDA LOS DATOS DE LA CARD Y PLAN DE PAGO
     *
     */
    public void payCard(final Activity activity, final List<String> tarjeta, final Map<String, String> datos, final DataCallback dataCallback) {
        progressStat(null);

<<<<<<< HEAD
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat("No se pudó procesar el pago, intentalo nuevamente.");
                        dataCallback.onResult(false);
                    }
                }
        ) {
=======
        Conekta.setPublicKey("key_HBVDq8eR2EPrzksdrrq3DYQ");
        Conekta.setApiVersion("1.0.0");
        Conekta.collectDevice(activity);

        Token token = new Token(activity);
        Card card = new Card(tarjeta.get(0), tarjeta.get(1), tarjeta.get(2), tarjeta.get(3), tarjeta.get(4));

        token.onCreateTokenListener(new Token.CreateToken() {
>>>>>>> origin/master
            @Override
            public void onCreateTokenReady(JSONObject data) {
                try {
                    datos.put(Constantes.PayKeys.token, data.getString("id"));
                    StringRequest stringRequest = new StringRequest(
                            Method.POST,
                            WebServices.registreCard,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    JsonObject objResponse = ((new JsonParser()).parse(response.toString())).getAsJsonObject();
                                    if (Integer.parseInt(objResponse.get("success").toString()) == 1) {
                                        List<String> datosPay = Validaciones.respuestaPay(null, response);
                                        SharedPreferences userDetails = contexto.getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                        SharedPreferences.Editor edit = userDetails.edit();
                                        userDetails.edit().putString("referenciaCargo", datosPay.get(2).trim()).apply();
                                        userDetails.edit().putString("flagFueRegistro", "1").apply();
                                        userDetails.edit().putString("activo", "1").apply();
                                        edit.commit();
                                        dataCallback.onResult(true);
                                    } else {
                                        alertDialogStat(objResponse.get("error").toString());
                                        dataCallback.onResult(false);
                                    }
                                    progressStat(null);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressStat(null);
                                    alertDialogStat("No se pudo procesar el pago, intentelo nuevamente.");
                                    dataCallback.onResult(false);
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            return datos;
                        }
                    };
                    stringRequest.setShouldCache(false);
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } catch (Exception err) {
                    err.printStackTrace();
                    progressStat(null);
                    dataCallback.onResult(false);
                }
            }
        });

        token.create(card);
    }


    /**
     * OBTIENE LA LISTA DE TIPS
     *
     * @param data
     * @param dataCallback
     * @return
     */
    public void getTips(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.tips,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Tips", response)) {
                            case 0:
                                alertDialogStat("Error al obtener lista de tips");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.r = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("iduser", data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * ENVÍA EL CUESTIONARIO DE PRIMERA VEZ
     *
     * @param data         CONTIENE TODAS LAS PREGUNTAS
     * @param dataCallback
     */
    public void cuestionario(final List<String> data, final DataCallback dataCallback) {

        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.cuestionario,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Cuestionario", response)) {
                            case 0:
                                alertDialogStat("Error al guardar los datos");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();
                HashMap<String, String> preguntasMap = new HashMap<String, String>();


                preguntasMap.put("edad", data.get(0));
                preguntasMap.put("genero", data.get(1));
                preguntasMap.put("estatura", data.get(2));
                preguntasMap.put("pesoMaximo", data.get(3));
                preguntasMap.put("pesominimo", data.get(4));
                preguntasMap.put("pesoActual", data.get(5));
                preguntasMap.put("pesoDeseado", data.get(6));
                preguntasMap.put("pesoSobrePeso", data.get(7));
                preguntasMap.put("padecimientos", data.get(8));
                preguntasMap.put("medicamentosDiarios", data.get(9));
                preguntasMap.put("estrenimiento", data.get(10));
                preguntasMap.put("actividadFisica", data.get(11));
                preguntasMap.put("ejercicioNoA", data.get(12));
                preguntasMap.put("fuma", data.get(13));
                preguntasMap.put("bebidasA", data.get(14));
                preguntasMap.put("aguaXdia", data.get(15));
                preguntasMap.put("orina", data.get(16));
                preguntasMap.put("medicamentosBajaPeso", data.get(17));
                preguntasMap.put("medicamentosBajaPesoC", data.get(18));
                preguntasMap.put("enfermedadesHered", data.get(19));
                preguntasMap.put("sindromePre", data.get(20));
                preguntasMap.put("reglasRegular", data.get(21));
                preguntasMap.put("pesoEmbarazo", data.get(22));
                preguntasMap.put("pesoEmbarazoAnte", data.get(23));
                preguntasMap.put("tratamientoHorm", data.get(24));
                preguntasMap.put("disgustaA", data.get(25));
                preguntasMap.put("disgustaAC", data.get(26));
                preguntasMap.put("vegetariano", data.get(27));
                preguntasMap.put("alimentosPredilec", data.get(28));
                preguntasMap.put("alimentosChatarra", data.get(29));
                preguntasMap.put("patrones", data.get(30));
                preguntasMap.put("compulsivo", data.get(31));
                preguntasMap.put("deprimido", data.get(32));
                preguntasMap.put("bebidasACuales", data.get(33));

                JSONObject obj = new JSONObject(preguntasMap);
                String jsonString = obj.toString();

                params.put(Constantes.CuestionarioKeys.cuestionario, jsonString);
                params.put(Constantes.CuestionarioKeys.usuario, data.get(34));
                params.put(Constantes.CuestionarioKeys.identificador, "1");

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * SAVE THE ANSWERS OF WEEK QUESTIONARY
     *
     * @param data
     * @param dataCallback
     */
    public void cuestionarioWeek(final List<String> data, final DataCallback dataCallback) {

        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.cuestionarioS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("CuestionarioSemanal", response)) {
                            case 0:
                                alertDialogStat("Error al guardar los datos");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();
                HashMap<String, String> preguntasMap = new HashMap<String, String>();


                preguntasMap.put("seguimiento", data.get(0));
                preguntasMap.put("gustoDieta", data.get(1));
                preguntasMap.put("banioDieta", data.get(2));
                preguntasMap.put("orinaDieta", data.get(3));
                preguntasMap.put("orinaDensaDieta", data.get(4));
                preguntasMap.put("resequedadBoca", data.get(5));
                preguntasMap.put("saborBoca", data.get(6));
                preguntasMap.put("malestar", data.get(7));
                preguntasMap.put("cualMalestar", data.get(8));
                preguntasMap.put("medidasCorporales", data.get(9));
                preguntasMap.put("ansiedad", data.get(10));
                preguntasMap.put("satisfecho", data.get(11));
                preguntasMap.put("tractoDigestivo", data.get(12));
                preguntasMap.put("distincion", data.get(13));
                preguntasMap.put("percibesSaciedad", data.get(14));
                preguntasMap.put("esfuerzoSeguir", data.get(15));
                preguntasMap.put("formaVida", data.get(16));
                preguntasMap.put("idcat", data.get(17));
                JSONObject obj = new JSONObject(preguntasMap);
                String jsonString = obj.toString();

                params.put(Constantes.CuestionarioKeys.cuestionario, jsonString);
                params.put(Constantes.CuestionarioKeys.identificador, "1");
                //params.put(Constantes.CuestionarioKeys.usuario, data.get(16));

                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * OBTIENE TODOS LOS DATOS PARA LLENAR LA GRAFICA DE TRAYECTORIA
     *
     * @param data
     * @param dataCallback
     */
    public void getDataChart(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.chart,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Data Chart", response)) {
                            case 0:
                                alertDialogStat("Error al obtener datos trayectoria");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.arrayDataChart = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * OBTIENE LOS DATOS PARA LA DIETA
     *
     * @param data
     * @param dataCallback
     */
    public void getDataDiet(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.dieta_info,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Información dieta", response)) {
                            case 0:
                                JsonObject object = ((new JsonParser()).parse(r)).getAsJsonObject();
                                alertDialogStat(object.get("extra").getAsString());
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.dataDiet = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("dieta", data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * METODO QUE OBTIENE LAS RECOMENDACIONES
     *
     * @param data
     * @param dataCallback
     */
    public void getDataRecCatalogo(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.info_catalogo,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Información catalogo recomendaciones", response)) {
                            case 0:
                                alertDialogStat("Error al obtener catalogo recomendaciones");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.dataDietCatalogo = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("dieta", data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * OBTIENE EL MENU RECOMENDADO DEL DÍA
     *
     * @param data
     * @param dataCallback
     */
    public void getDataMenu(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.menu,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Información menu", response)) {
                            case 0:
                                alertDialogStat("No se pudo obtener el menú de la dieta o no tiene menú asigando.");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.dataDietMenu = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("dieta", data.get(0));
                params.put("dia", data.get(1));
                params.put("idcat", data.get(2));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    /**
     * RECIBE EL ID DE RECETA Y OBTIENE TODOS LOS DETALLES DE ESTA
     *
     * @param data
     * @param dataCallback
     */
    public void getDataReceta(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.receta,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        r = response.toString();
                        progressStat(null);
                        switch (Validaciones.respuestaSencilla("Información receta", response)) {
                            case 0:
                                alertDialogStat("Error al obtener receta");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                GlobalVars.dataDietReceta = r;
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idreceta", data.get(0));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    /**
     * ENVÍA LAS CONFIGURACIONES DE NOTIF Y PAGO AL SERVER
     *
     * @param data
     * @param dataCallback
     */
    public void configNotifications(final List<String> data, final DataCallback dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.notifications,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);

                        switch (Validaciones.respuestaSencilla("Notificaciones", response)) {
                            case 0:
                                alertDialogStat("Error al guardar configuraciones, intente nuevamente!");
                                dataCallback.onResult(false);
                                break;
                            case 1:
                                dataCallback.onResult(true);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(IDCAT, data.get(0));
                params.put(NOTIFICATIONS, data.get(1));
                params.put(CHARGE, data.get(2));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 2, 2));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void cambiarAvatar(final String id, final Bitmap imagen, final DataCallback.Extra dataCallback) {
        progressStat("Subiendo Imagen");
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(
                Method.POST,
                WebServices.subirImagen,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        String resultResponse = new String(response.data);
                        switch (Validaciones.respuestaSencilla("resultado", resultResponse)) {
                            default:
                                alertDialogStat(Validaciones.respuestaImagen(null, resultResponse));
                                dataCallback.onResult(false, "");
                                break;
                            case 1:
                                dataCallback.onResult(true, Validaciones.respuestaImagen(null, resultResponse));
                                break;
                        }
                        progressStat(null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", id);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                params.put("image", new DataPart("file_avatar.jpg", Utilidades.getFileDataFrom(imagen), "image/jpeg"));
                return params;
            }
        };
        volleyMultipartRequest.setShouldCache(false);
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(volleyMultipartRequest, WebServices.tag_login);
    }

    public void citas(final String idusuario, final DataCallback.ExtraValue dataCallback) {
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.citas,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dataCallback.onResult(true, Validaciones.respuestaExtra(null, response), Validaciones.respuestaSencilla("respuesta", response));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dataCallback.onResult(false, "", -1);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", idusuario);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, WebServices.tag_login);
    }

    public void agendar(final List<String> data, final DataCallback.Extra dataCallback) {
        progressStat("Agendando...");
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.agendar,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        switch (Validaciones.respuestaSencilla(null, response)) {
                            default:
                                dataCallback.onResult(false, Validaciones.respuestaExtra(null, response));
                                break;
                            case 1:
                                dataCallback.onResult(true, Validaciones.respuestaExtra(null, response));
                                break;
                        }
                        progressStat(null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false, "");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", data.get(0));
                params.put("fecha_cita", data.get(1));
                params.put("hora_cita", data.get(2));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, WebServices.tag_login);
    }

    public void chat(final List<String> data, final boolean anima, final DataCallback.ExtraAny dataCallback) {
        if (anima)
            progressStat("Obteniendo Chat...");

        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.chat,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (data.get(1).equals("1"))
                            switch (Validaciones.respuestaSencilla(null, response)) {
                                case 1:
                                    dataCallback.onResult(true, Validaciones.respuestaChat(response));
                                    break;
                                default:
                                    alertDialogStat("Error");
                                    dataCallback.onResult(false, null);
                                    break;
                            }
                        else {
                            Validaciones.respuestaSencilla(null, response);
                            dataCallback.onResult(true, Validaciones.respuestaChat(response));
                        }
                        if (anima)
                            progressStat(null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false, null);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("idusuario", data.get(0));
                params.put("guardar", data.get(1));
                params.put("mensaje", data.get(2));
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, WebServices.tag_login);
    }

    public void logout(final String data, final DataCallback dataCallback) {
        progressStat("Saliendo...");
        StringRequest stringRequest = new StringRequest(
                Method.POST,
                WebServices.logout,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dataCallback.onResult(true);
                        progressStat(null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressStat(null);
                        alertDialogStat(contexto.getResources().getString(R.string.error_web_services));
                        Log.e(label, error.toString());
                        dataCallback.onResult(false);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("usuario", data);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, WebServices.tag_login);
    }
}