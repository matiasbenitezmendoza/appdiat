package com.diat.app.navigation;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.auxclases.NavListModel;
import com.diat.app.auxclases.firebase.FirebaseKeys;
import com.diat.app.auxclases.firebase.NotificationUtils;
import com.diat.app.diat.MainActivity;
import com.diat.app.diat.R;
import com.diat.app.registro.FCuestionarioUno;
import com.diat.app.registro.FPago;
import com.diat.app.registro.FTipoPlan;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import com.diat.app.webservices.AppController;
import com.diat.app.webservices.Servicios;

import static com.diat.app.auxclases.Constantes.UrlImgs.urlImg;

/**
 * Created by webandando on 30/11/16.
 */

public class LandingActivityB extends AppCompatActivity {

    private ScrimInsetsFrameLayout sifl;
    private DrawerLayout drawerLayout = null;
    private Toolbar toolbar;
    private String inforSatrt;
    private boolean doubleBackToExitPressedOnce = false;
    private FragmentManager manager;
    private ImageLoader imageLoader;
    private Dialog customDialog = null;
    private SharedPreferences sharedPreferences;
    private ImageView imgNavigat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = getSupportFragmentManager();
        imageLoader = AppController.getInstance().getImageLoader();
        sharedPreferences = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        /**if es un user add by admin redirect to questionary 1 */
        if (!sharedPreferences.getString("idCompania", "").equals("null") && sharedPreferences.getString("cuestionario", "").equals("null")) {
            setContentView(R.layout.activity_principal);
            manager
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .replace(R.id.flInicio, new FCuestionarioUno(), "toFCuestionarioUno")
                    .commit();
        } else {
            setContentView(R.layout.conten_layout_b);
            setDrawer();
            fragment_welcome();
        }

    }


    @Override
    protected void onResume() {
        Log.e("Firebase", getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE).getAll().toString());
        int i = getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE).getInt("tipo", -1);
        switch (i) {
            default:
                fragment_welcome();
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case 1:
                fragment_tips(1);
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case 2:
                new Servicios(this).getInfoInicio(Arrays.asList(
                        sharedPreferences.getString("idcat", "")
                        ),

                        new Servicios.DataCallback() {
                            @Override
                            public void onResult(boolean result) {
                                if (result) {
                                    inforSatrt = GlobalVars.infoStart;
                                    fillFields();
                                    fragment_dieta();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case 3:
                SharedPreferences userDetails = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                SharedPreferences.Editor edit = userDetails.edit();
                userDetails.edit().putString("activo", "0").apply();
                edit.commit();
                //setDrawer();
                //fragment_welcome();
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                showPayDialog();
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case 4:
                manager
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.flInicio, new FChat())
                        .addToBackStack(null)
                        .commit();
                manager
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.flInicio, new FConversacion())
                        .addToBackStack(null)
                        .commit();
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case 5:
                fragment_chat();
                getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                        .edit().putInt("tipo", -1).apply();
                break;
            case -1:
                break;
        }
        NotificationUtils.clearNotifications(getApplicationContext());
        super.onResume();
    }

    private void fillFields() {
        JSONObject obj;
        try {
            obj = new JSONObject(inforSatrt);
            String dieta = obj.getString("dieta");
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("iddieta", String.valueOf(dieta)).apply();
            edit.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void inicio_welcome(View view) {
        fragment_welcome();
        drawerLayout.closeDrawer(sifl);//descomentar para que no se cierre cuando este logueado
        toolbar.setBackgroundResource(R.color.gris_toolbar);
    }

    private void fragment_welcome() {
        manager.popBackStack();
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FWelcome())
                .commit();
    }

    private void fragment_mainConsulta() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FMainConsulta())
                //.addToBackStack(null)
                .commit();
    }


    private void fragment_dieta() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FDietaWeek())
                //.addToBackStack(null)
                .commit();
    }


    private void fragment_chat() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FChat())
                //.addToBackStack(null)
                .commit();
    }

    private void fragment_trayectoria() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FTrayectoria())
                //.addToBackStack(null)
                .commit();
    }


    private void fragment_pesate() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FPesate())
                //.addToBackStack(null)
                .commit();

    }

    //Este es llamdo desde el boton del dash
    public void fragment_peso(View view) {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FPesate())
                .addToBackStack(null)
                .commit();
    }


    public void logeo(View view) {
        /*manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FWelcome(), "toFLogin")
                .addToBackStack(null)
                .commit();*/
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FWelcome())
                .commit();

    }

    private void fragment_nosotros() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FNosotros())
                //.addToBackStack(null)
                .commit();

    }


    private void fragment_nuestrosProductos(int logueado) {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FNuestrosProductos())
                //.addToBackStack(null)
                .commit();
    }

    private void fragment_config() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FConfiguracion())
                //.addToBackStack(null)
                .commit();

    }

    private void fragment_tips(int logueado) {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FTips())
                //.addToBackStack(null)
                .commit();
    }

    private void fragment_pago() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FPago(), "NewFragmentPay")
                .commit();
    }

    private void fragment_tipoPlan() {
        manager
                .beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.flInicio, new FTipoPlan(), "FTipoPlan")
                .commit();


    }


    private void setDrawer() {
        sifl = (ScrimInsetsFrameLayout) findViewById(R.id.scrimInsentsFrameLayout);
        toolbar = (Toolbar) findViewById(R.id.include);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawLanding);
        ListView listView = (ListView) findViewById(R.id.navdrawerlist);

        NavAdapter adapter = new NavAdapter();

        if (listView != null) {
            listView.setAdapter(adapter);
        }
        String name = sharedPreferences.getString("name", "");
        String lastNamne = sharedPreferences.getString("lastName", "");
        String imagen = sharedPreferences.getString("imagen", "");


        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.navigation_header_navlist, listView, false);
        listView.addHeaderView(header, null, false);
        TextView txUser = (TextView) findViewById(R.id.txNombreUser);
        txUser.setText(name + " " + lastNamne);

        imgNavigat = (ImageView) findViewById(R.id.imgNavigation);
        String url = urlImg + imagen;
        imageLoader.get(url, ImageLoader.getImageListener(imgNavigat, R.drawable.img_genericperson, R.drawable.img_genericperson));


        setSupportActionBar(toolbar);
        drawerLayout.setStatusBarBackgroundColor(
                getResources().getColor(R.color.gris_toolbar));

        ImageView hamburgerMenu = (ImageView) toolbar.findViewById(R.id.hamburgerMenu);
        hamburgerMenu.setImageResource(R.drawable.menu_hamburger);
        hamburgerMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawerLayout.isDrawerOpen(sifl)) {
                    drawerLayout.openDrawer(sifl);
                } else {
                    drawerLayout.closeDrawer(sifl);
                }
            }
        });
        toolbar.setBackgroundResource(R.drawable.layer_list_toolbar);
    }


    public class NavAdapter extends BaseAdapter {

        private ArrayList<NavListModel> lista;
        private LayoutInflater inflater = null;
        public Resources res;


        SharedPreferences sharedPreferences = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        private String isLogedUser = sharedPreferences.getString("isLoged", "");


        NavAdapter() {
            lista = new ArrayList<>();

            if (isLogedUser.equals("1")) {
                //lista.add(new NavListModel(R.drawable.img_genericperson, "NOMBRE"));//AGREGARLA EN UN HEADER
                lista.add(new NavListModel(R.drawable.ico_consultya, "   CONSULTA"));
                lista.add(new NavListModel(R.drawable.ico_menu, "   DIETA DE LA SEMANA"));
                lista.add(new NavListModel(R.drawable.ico_menutrayectoria, "   TRAYECTORIA"));

                String plan = sharedPreferences.getString("tipoPlan", "");
                switch (plan){
                    case "VIP": plan = "   ACCESO VIP"; break;
                    case "Audaz": plan = "   ACCESO TELEFÓNICO"; break;
                    default: plan = "   ACCESO AL CHAT"; break;
                }


                lista.add(new NavListModel(R.drawable.ico_acceso, plan));

                lista.add(new NavListModel(R.drawable.ico_pesate, "   ¡PÉSATE!"));
                lista.add(new NavListModel(R.drawable.ico_nosotros, "   ACERCA DE NOSOTROS"));
                lista.add(new NavListModel(R.drawable.ico_tips, "   TIPS"));
                lista.add(new NavListModel(R.drawable.ico_menuproducts, "   NUESTROS PRODUCTOS"));
                lista.add(new NavListModel(R.drawable.ico_promo, "   CONFIGURACIÓN"));
                lista.add(new NavListModel(R.drawable.ico_shutdown, "   CERRAR SESIÓN"));

            } else {
                lista.add(new NavListModel(R.drawable.ico_tips, "   TIPS"));
                lista.add(new NavListModel(R.drawable.ico_menuproducts, "   NUESTROS PRODUCTOS"));
            }

            inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint({"ViewHolder", "InflateParams"})
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi;
            NavAdapter.Holder holder = new NavAdapter.Holder();
            vi = inflater.inflate(R.layout.adapter_nav_item, null);
            holder.imgNav = (ImageView) vi.findViewById(R.id.imgNav);
            holder.tvNav = (TextView) vi.findViewById(R.id.tvNav);


            holder.imgNav.setImageResource(lista.get(position).getImagen());
            holder.tvNav.setText(lista.get(position).getNombre());

            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavClick(position);
                }
            });
            return vi;
        }

        class Holder {
            ImageView imgNav;
            TextView tvNav;
        }
    }


    public void NavClick(int position) {
        //String activo = sharedPreferences.getString("activo", "");

        SharedPreferences sPreferences = getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        String activo = sPreferences.getString("activo", "");

        switch (position) {
            case 0:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_mainConsulta();
                } else {
                    showPayDialog();
                }
                break;
            case 1:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_dieta();
                } else {
                    showPayDialog();
                }
                break;
            case 2:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_trayectoria();
                } else {
                    showPayDialog();
                }
                break;
            case 3:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_chat();
                } else {
                    showPayDialog();
                }
                break;
            case 4:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_pesate();
                } else {
                    showPayDialog();
                }
                break;
            case 5:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                fragment_nosotros();
                break;
            case 6:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                if (activo.equals("1")) {
                    fragment_tips(1);
                } else {
                    showPayDialog();
                }
                break;
            case 7:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                fragment_nuestrosProductos(1);
                break;
            case 8:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                fragment_config();
                break;
            case 9:
                drawerLayout.closeDrawer(sifl);
                toolbar.setBackgroundResource(R.color.gris_toolbar);
                new Servicios(this).logout(sharedPreferences.getString("idcat", ""),
                        new Servicios.DataCallback() {
                            @Override
                            public void onResult(boolean result) {
                                if (result) {

                                    sharedPreferences.edit().clear().apply();
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            }
                        });
                break;


        }
    }

    @Override
    public void onBackPressed() {
        FCuestionarioUno fCuestionarioUno = (FCuestionarioUno) getSupportFragmentManager().findFragmentByTag("toFCuestionarioUno");


        if (fCuestionarioUno != null && fCuestionarioUno.isVisible()) {
            //no hace nada cuando viene el back de custionario uno cuando el usuario ocupo ese fragment
            //es usuario agregado by admin
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getText(R.string.atras_para_salir), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
            }
        } else {
            if (drawerLayout.isDrawerOpen(sifl)) {
                drawerLayout.closeDrawer(sifl);
            } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getText(R.string.atras_para_salir), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                super.onBackPressed();
            }
        }

    }


    private void showPayDialog() {
        customDialog = new Dialog(this);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.dialog);
        TextView titulo = (TextView) customDialog.findViewById(R.id.titulo);
        titulo.setText("Pago Pendiente");

        TextView contenido = (TextView) customDialog.findViewById(R.id.contenido);
        contenido.setText("Su pago no ha sido procesado");

        ((Button) customDialog.findViewById(R.id.btnrenovar_plan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment_pago();
                customDialog.dismiss();
            }
        });

        ((Button) customDialog.findViewById(R.id.btncambiar_plan)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment_tipoPlan();
                customDialog.dismiss();
            }
        });
        ((Button) customDialog.findViewById(R.id.btnotro_moment)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    public void actualizarFoto() {
        String url = urlImg + getSharedPreferences("PreferencesRegisto", MODE_PRIVATE).getString("imagen", "");
        imageLoader.get(url, ImageLoader.getImageListener(imgNavigat, R.drawable.img_genericperson, R.drawable.img_genericperson));
    }


}
