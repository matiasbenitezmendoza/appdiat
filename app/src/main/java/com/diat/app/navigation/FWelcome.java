package com.diat.app.navigation;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.diat.app.auxclases.Constantes;
import com.diat.app.auxclases.CustomTypefaceSpan;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;
import com.diat.app.registro.FPago;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import com.diat.app.webservices.AppController;
import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.diat.app.auxclases.Constantes.UrlImgs.urlBanner;
import static com.diat.app.auxclases.Constantes.UrlImgs.urlImg;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FWelcome extends Fragment {

    private View view;
    private String inforSatrt;
    private String idusuario;
    private String nombre = "";
    private String diasHace;
    private double ultimoPeso = 0.0;
    private String dias;
    private String peso_total;
    private double pesoActual = 0, pesoDeseado = 0;
    private double avance = 0.0;
    private String proxima_consulta = "0";
    private String dieta, activo, banner, activoNotif;
    private SharedPreferences sharedPreferences;
    private ImageView imageView;
    private ImageLoader imageLoader;
    private ImageView imgBannner;
    private Typeface OswaldBoldfont;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_welcome, container, false);
        imageLoader = AppController.getInstance().getImageLoader();
        showHamburger();
        OswaldBoldfont = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.bold.ttf");

        sharedPreferences = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        idusuario = sharedPreferences.getString("idcat", "");
        nombre = sharedPreferences.getString("name", "") + " " + sharedPreferences.getString("lastName", "");
        activo = sharedPreferences.getString("activo", "");


        getStartInfo();

        //if the not pay
        if(activo.equals("0")){
            Button btnRegiterWeigth = (Button) view.findViewById(R.id.btnRegiterWeigth);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            btnRegiterWeigth.setEnabled(false);
            imageView.setEnabled(false);
        }

        hideToolCenter();

        return view;
    }



    public void hideToolCenter() {
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(INVISIBLE);
        toolR.setVisibility(VISIBLE);
    }




    private void buttonClose() {

        ImageButton btnClose = (ImageButton) view.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout ly_promo = (RelativeLayout) view.findViewById(R.id.ly_promo);
                ly_promo.setVisibility(View.GONE);

                resizeScreen();
            }
        });


    }

    private void displayBanner() {

        imgBannner = (ImageView) view.findViewById(R.id.imgBanner);
        String url = urlBanner + banner;
        imageLoader.get(url, ImageLoader.getImageListener(imgBannner, 0, 0));

    }


    private void getStartInfo() {
        new Servicios(getContext()).getInfoInicio(Arrays.asList(
                idusuario
                ),
                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {
                            inforSatrt = GlobalVars.infoStart;
                            fillFields();

                            sharedPreferences = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                            banner = sharedPreferences.getString("banner", "");
                            if (banner != ""){
                                displayBanner();
                                buttonClose();
                            }



                        } else {
                            Toast.makeText(getApplicationContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private void fillFields() {
        double x1 = 0;
        double x2 = 0;
        JSONObject obj = null;
        try {
            obj = new JSONObject(inforSatrt.toString());
            peso_total = obj.getString("peso_total");
            dieta = obj.getString("dieta");
            JSONObject result = obj.getJSONObject("ultimo_peso");
            diasHace = result.getString("dias");
            ultimoPeso = Double.parseDouble(result.getString("peso"));
            proxima_consulta = obj.getString("proxima_consulta");


            //if(obj.getBoolean("user_peso") != false){
            JSONObject user_peso = obj.getJSONObject("user_peso");
            if (!user_peso.getString("pesoActual").equals("") && !user_peso.getString("pesoDeseado").equals("")) {
                pesoActual = Double.parseDouble(user_peso.getString("pesoActual"));
                pesoDeseado = Double.parseDouble(user_peso.getString("pesoDeseado"));

                if(ultimoPeso == 0){
                    ultimoPeso = pesoActual;
                }
                //ACT - DES = X1  ,  ULT - DES = X2  ,  AVANCE = ? = X2 * 100 / X1
                x1 = pesoActual - pesoDeseado;
                x2 = ultimoPeso - pesoDeseado;
                avance = ((x1 - x2) * 100) / (x1);
            }
           //}




            /*SharedPreferences userDetails = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                SharedPreferences.Editor edit = userDetails.edit();
                userDetails.edit().putString("proxima_consulta",  proxima_consulta).apply();
                userDetails.edit().putString("last_weigth",  String.valueOf(ultimoPeso)).apply();
                userDetails.edit().putString("advance",  String.valueOf(avance)).apply();
                userDetails.edit().putString("iddieta",  String.valueOf(dieta)).apply();
                userDetails.edit().putString("last_weigth_days",  String.valueOf(diasHace)).apply();
                userDetails.edit().putString("weigth_actual",  String.valueOf(pesoActual)).apply();
                userDetails.edit().putString("peso_total_comunidad",  String.valueOf(peso_total)).apply();
                userDetails.edit().putString("pesoDeseado",  String.valueOf(pesoDeseado)).apply();
            edit.commit();*/


            dias = obj.getString("dias");
            proxima_consulta = obj.getString("proxima_consulta");
            sharedPreferences.edit()
                    .putString("proxima_consulta", proxima_consulta)
                    .putString("last_weigth", String.valueOf(ultimoPeso))
                    .putString("advance", String.valueOf(avance))
                    .putString("last_weigth_days", String.valueOf(diasHace))
                    .putString("dias", String.valueOf(dias))

                    .putString("iddieta", String.valueOf(dieta))
                    .putString("weigth_actual", String.valueOf(pesoActual))
                    .putString("peso_total_comunidad", String.valueOf(peso_total))
                    .putString("pesoDeseado", String.valueOf(pesoDeseado))
                    .apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView txInicioUno = (TextView) view.findViewById(R.id.txInicioUno);
        txInicioUno.setText(peso_total + " kg");

        /*
        * 2do texto de pantalla de incio
        */
        TextView txInicioDos = (TextView) view.findViewById(R.id.txInicioDos);
        String txt = "Este es el día "+dias+" desde que comenzaste tu programa Diät";

        int startingIndex = txt.indexOf("día");
        final SpannableStringBuilder str = new SpannableStringBuilder(txt);
        //str.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, (startingIndex+7), 0);
        str.setSpan(new CustomTypefaceSpan("", OswaldBoldfont), startingIndex, (startingIndex+7), 1);

        startingIndex = txt.indexOf("Diät");
        str.setSpan(new CustomTypefaceSpan("", OswaldBoldfont), startingIndex, (startingIndex+4), 1);
        txInicioDos.setText(str);

        /*
        * 3er texto de pantalla de incio
        */
        String ultPeso = String.valueOf(ultimoPeso);
        String txt_peso = "Hace " + diasHace + " día(s) tu peso fue de "+ultPeso+" kg.";
        final SpannableStringBuilder str_peso = new SpannableStringBuilder(txt_peso);

        int startingIndex_peso = txt_peso.indexOf(ultPeso);
        int endingIndex_peso = startingIndex_peso+ultPeso.length()+4;
        str_peso.setSpan(new CustomTypefaceSpan("", OswaldBoldfont), startingIndex_peso, endingIndex_peso, 1);
        TextView tx_days_kgs = (TextView) view.findViewById(R.id.tx_days_kgs);
        tx_days_kgs.setText(str_peso);


        TextView txBienvenido = (TextView) view.findViewById(R.id.txBienvenido);
        txBienvenido.setText("Bienvenid@ " + nombre);

        imageView = (ImageView) view.findViewById(R.id.imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFoto().show();
            }
        });

        cargarImagen();
        TextView txPAct = (TextView) view.findViewById(R.id.pesActual);
        txPAct.setText(pesoActual + " kg");
        TextView txPDes = (TextView) view.findViewById(R.id.pesDeseado);
        txPDes.setText(pesoDeseado + " kg");

        ProgressBar simpleProgressBar = (ProgressBar) view.findViewById(R.id.progressBar); // initiate the progress bar
        simpleProgressBar.setMax(100); // 100 maximum value for the progress value
        simpleProgressBar.setProgress((int) avance); // 50 default progress value for the progress bar
        int color = 0xFF21A0DB;
        simpleProgressBar.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        simpleProgressBar.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }


    private void resizeScreen() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        LinearLayout llWelcome;
        //llWelcome = (FrameLayout) getActivity().findViewById(R.id.flInicio);
        llWelcome = (LinearLayout) view.findViewById(R.id.lyResizable);
        llWelcome.getLayoutParams().height = height;
    }

    public AlertDialog cambiarFoto() {
        final CharSequence[] opciones = {"Camara", "Galeria"};
        AlertDialog.Builder dialogo = new AlertDialog.Builder(getContext())
                .setCancelable(true)
                .setItems(opciones, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                openCamera();
                                break;
                            case 1:
                                openGallery();
                                break;
                        }
                    }
                });

        return dialogo.create();
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, Constantes.PHOTO_CODE);
    }

    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, Constantes.SELECTED_PICTURE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constantes.PHOTO_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri path = data.getData();
                    performCrop(path);
                }
                break;


            case Constantes.SELECTED_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri path = data.getData();
                    performCrop(path);
                }
                break;

            case Constantes.RESULT_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap selectedBitmap = extras.getParcelable("data");

                    new Servicios(getContext()).cambiarAvatar(
                            idusuario,
                            selectedBitmap,
                            new Servicios.DataCallback.Extra() {
                                @SuppressLint("CommitPrefEdits")
                                @Override
                                public void onResult(boolean result, String extra) {
                                    if (result) {
                                        sharedPreferences.edit().putString("imagen", extra).commit();
                                        cargarImagen();
                                        ((LandingActivityB) getActivity()).actualizarFoto();
                                    }
                                }
                            }
                    );
                    //Bitmap selectedBitmap = extras.getParcelable("data");
                    // Set The Bitmap Data To ImageView
                    //      actualizarImagen(selectedBitmap);
                }
                break;
        }
    }

    private void cargarImagen() {
        String url = urlImg + sharedPreferences.getString("imagen", "");
        imageLoader.get(url, ImageLoader.getImageListener(imageView, R.drawable.img_genericperson, R.drawable.img_genericperson));
    }

    private void performCrop(Uri picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 300);
            cropIntent.putExtra("outputY", 300);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, Constantes.RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void fragment_pago(){
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.flInicio, new FPago(), "NewFragmentPay");
        ft.commit();
    }

    private void showHamburger(){
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(VISIBLE);
    }

}
