package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.diat.app.auxclases.AdapterNuestrosProductos;
import com.diat.app.diat.R;

import static android.content.Context.MODE_PRIVATE;


public class FNuestrosProductos extends Fragment {

    private View view;
    private String[] titulo;
    private String[] contenido;
    private Integer[] imgid;

    public FNuestrosProductos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_nuestrosproductos, container, false);


        titulo = new String[]{"Diät Project", "Diät Mind",  "Diät Body", "Diät Spirit"};
        contenido = new String[]{"Infórmate y lee todo acerca de la filosofía de Diät. Aplícate el test.\nDefine tu peso ideal según los lineamientos expuestos. Diseña tu dieta bajo el cobijo de este proyecto.\n" +
                "Implementa las dietas en  el orden sugerido para ti. Culmina con tu mantenimiento.\nImplementa técnicas de curación permanente.\n",
                "Complementa el libro principal con la terapia emocional.\nDespega el nexo entre las emociones y los impulsos por comer.\n",
                "Manual de ejercicio físico seguro, y eficiente. Es práctico, necesitas poco tiempo y los resultados serán evidentes a corto plazo.\nEncontrarás los lineamientos para hacer correctamente:\n" + "Calentamiento.\nEjercicio aeróbico.\nPrograma exclusivo, completo e innovador de resistencia.\nPráctica de estiramiento.\n",
                "Sistema de relajación y meditación Diät para que vivas en paz y disfrutes de la plenitud de tu vida.\nBeneficia a cualquier persona y la conecta con la fuente inagotable de energía y de luz.\n"};
        imgid= new Integer []{
                R.drawable.img_libroproject,
                R.drawable.img_libromind,
                R.drawable.img_librobody,
                R.drawable.img_librospirit
        };

        fillProductosList();

        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        String isLogedUser = varReferenceCargo.getString("isLoged", "");

        if(isLogedUser =="1") {
            resizeLayNuesProd();
        }



        return view;
    }



   private void fillProductosList(){

       AdapterNuestrosProductos adapter=new AdapterNuestrosProductos(getActivity(),titulo,imgid, contenido);
       ListView lista = (ListView) view.findViewById(R.id.lvNuestrosProducts);

       LayoutInflater inflater = getActivity().getLayoutInflater();
       ViewGroup header = (ViewGroup) inflater.inflate(R.layout.navigation_header_productos, lista, false);
       lista.addHeaderView(header, null, false);
       lista.setAdapter(adapter);
       /*lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String Slecteditem= titulo[+position];
               Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();
           }
       });*/


       }

    private void resizeLayNuesProd (){
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.llNuestrosProd);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(8, 5, 8, 40);
        //(leftMargin, topMargin, rightMargin, bottomMargin);
        layout.setLayoutParams(params);

    }


}
