package com.diat.app.navigation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.diat.app.auxclases.ExpandableListAdapter;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.diat.app.webservices.AppController;
import com.diat.app.webservices.Servicios;

import static com.diat.app.auxclases.Constantes.UrlImgs.urlImgRecetas;


public class FReceta extends Fragment {

    private View view;
    private String idreceta, dataDietReceta, txTimePreparar, txNameReceta, txImageReceta;
    private ImageLoader imageLoader;
    private ImageView imgNavigat;
    private List<String> listDataHeader;
    private List<String> listDataContent;
    private List<String> listDataBuffer;
    private HashMap<String, List<String>> listDataChild;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_receta, container, false);
        imageLoader = AppController.getInstance().getImageLoader();
        idreceta = getArguments().getString("idreceta");

        sendPetition();


        return view;
    }



    private void sendPetition(){

            new Servicios(getContext()).getDataReceta(Arrays.asList(
                    idreceta
                    ),

                    new Servicios.DataCallback() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {

                                dataDietReceta = GlobalVars.dataDietReceta;
                                //pushData();
                                parseData();

                                expandList();


                            }
                            else{

                            }
                        }
                    });


    }





    private void parseData(){

        String ingredientes="";
        listDataHeader = new ArrayList<String>();
        listDataContent = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("ingredientes");
        listDataHeader.add("preparacion");


        try {
            JSONObject jsonResponse = new JSONObject(dataDietReceta);
            JSONArray mainNode = jsonResponse.getJSONArray("receta");


            JSONObject jsonChildNode = mainNode.getJSONObject(0);
            txNameReceta = jsonChildNode.optString("nombre");
            txTimePreparar = jsonChildNode.optString("tiempo");
            txImageReceta = jsonChildNode.optString("imagen");

            for(int x = 0; x< listDataHeader.size(); x++) {
                JSONArray ingr = jsonChildNode.getJSONArray(listDataHeader.get(x));
                for (int i = 0; i < ingr.length(); i++) {
                    ingredientes += ingr.optString(i) + "\n";

                }
                listDataContent.add(ingredientes);
                ingredientes="";
            }


        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        for(int k=0; k < listDataHeader.size();  k++) {
            listDataBuffer = new ArrayList<String>();
            listDataBuffer.add(listDataContent.get(k));
            listDataChild.put(listDataHeader.get(k).toString(), listDataBuffer);
        }



    }


        private void expandList(){
            ExpandableListAdapter listAdapter;
            ExpandableListView expListView;
            // get the listview
            expListView = (ExpandableListView) view.findViewById(R.id.elvReceta);
            LayoutInflater inflater = getActivity().getLayoutInflater();
            ViewGroup header = (ViewGroup) inflater.inflate(R.layout.navigation_header_receta, expListView, false);
            expListView.addHeaderView(header, null, false);




            ////SET INFO TO HEADER VIEW
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            TextView txTitle = (TextView) view.findViewById(R.id.txTitle);
            txTitle.setText(txNameReceta);

            TextView txTime = (TextView) view.findViewById(R.id.txTime);
            txTime.setText("Tiempo de preparación " + txTimePreparar);

            imgNavigat = (ImageView) view.findViewById(R.id.imgReceta);
            String url = urlImgRecetas + txImageReceta;
            imageLoader.get(url, ImageLoader.getImageListener(imgNavigat, R.drawable.img_genericamenu, R.drawable.img_genericamenu));
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            for ( int i = 0; i < listDataHeader.size(); i++ ) {
                expListView.expandGroup(i);
            }

            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                /*if(groupPosition == 2) {
                    Toast.makeText(getContext() , "Group 2 clicked", Toast.LENGTH_LONG).show();
                    return true;
                } else*/
                    return true;
                }
            });




        }


}
