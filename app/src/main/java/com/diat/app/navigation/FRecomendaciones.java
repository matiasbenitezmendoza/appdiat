package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.diat.app.auxclases.AdapterLVRecomendaciones;
import com.diat.app.auxclases.ExpandableListAdapter;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.diat.app.registro.ListVATipoPlan;
import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;


public class FRecomendaciones extends Fragment {

    private View view;
    private String titleRecomendaciones, iddieta, dataRecomendaciones, especificaciones, dataDietCatalogo;
    String[] AlistDataHeader;
    String[] AlistDataContent;
    private AdapterLVRecomendaciones adapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_recomendaciones, container, false);


        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        iddieta  = varReferenceCargo.getString("iddieta", "");
        titleRecomendaciones = getArguments().getString("titleRecomendaciones");
        fillList();

        return view;
    }






    private void fillList(){


        new Servicios(getContext()).getDataRecCatalogo(Arrays.asList(
                iddieta
                ),

                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {

                            dataDietCatalogo = GlobalVars.dataDietCatalogo;
                            fillexpList();

                        }
                        else{

                        }
                    }
                });

    }







    private void fillexpList(){

        ListView expListView;
        expListView = (ListView) view.findViewById(R.id.elvRecomendaciones);

        prepareListData();

        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.navigation_header_recomendaciones, expListView, false);
        expListView.addHeaderView(header, null, false);

        /////////////////////////////////////////////////////////////
        //Llena primer apartado de la lista header "especificaciones"
        dataRecomendaciones = GlobalVars.dataDiet;

               try {
                    JSONObject obj = new JSONObject(dataRecomendaciones);
                    JSONObject result = obj.getJSONObject("dieta");
                    JSONObject nom = result.getJSONObject("info");

                    especificaciones = nom.getString("especificaciones");

                   TextView txTitle = (TextView) view.findViewById(R.id.txHeaderRecomendaciones);
                   txTitle.setText(especificaciones);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
        ////////////////////////////////////////////////////////////

        adapter = new AdapterLVRecomendaciones(this.getContext(), AlistDataHeader , AlistDataContent);

        expListView.setAdapter(adapter);

    }




    private void prepareListData() {
        String o ="";
        TextView titl_recomendaciones = (TextView) view.findViewById(R.id.titl_recomendaciones);
        titl_recomendaciones.setText(titleRecomendaciones);



        //llena a partir del segundo apartado de la lista de recomendaciones
        dataDietCatalogo = GlobalVars.dataDietCatalogo;
        try {
            JSONObject jsonResponse = new JSONObject(dataDietCatalogo);
            JSONArray nom = jsonResponse.getJSONArray("catalogos");

            AlistDataHeader = new String[nom.length()];
            AlistDataContent = new String[nom.length()];

            for(int i = 0; i<nom.length();i++) {
                JSONObject jsonChildNode = nom.getJSONObject(i);
                String nombre = jsonChildNode.optString("nombre_catalogo");
                String outPut = nombre;
                // Adding name of catalogues verduras, proteinas, etc
                AlistDataHeader[i]= outPut;

                JSONArray nombres = jsonChildNode.optJSONArray("info");
                int m = nombres.length() - 1;


                for (int j = 0; j < nombres.length(); j++) {

                    JSONObject jsonChildNodeT = nombres.getJSONObject(j);

                    String n = jsonChildNodeT.optString("nombre");

                    if (m == j) {
                        o += n+ ".";
                    } else {
                        o += n + ", ";
                    }
                }
                AlistDataContent[i] = o;
                o = "";

            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }



}
