package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;
import com.diat.app.webservices.Servicios;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FInfoPesate extends Fragment {

    private View view;
    private String registerWeigth = "";
    private String peso_total_comunidad , ultimoPeso, weigth_actual, idcat;
    private String inforSatrt;
    private SharedPreferences sharedPreferences;
    private String diasHace;
    private double ultimoPesoD = 0.0;
    private String dias;
    private String peso_total;
    private double pesoActual = 0, pesoDeseado = 0;
    private double avance = 0.0;
    private String proxima_consulta = "0";
    private String dieta;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.navigation_frag_infpesate, container, false);


        SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        ultimoPeso = userDet.getString("last_weigth", "");//peso actual ingresado
        peso_total_comunidad = userDet.getString("peso_total_comunidad", "");
        weigth_actual = userDet.getString("weigth_actual", "");//peso inicial
        idcat = userDet.getString("idcat", "");


        registerWeigth = getArguments().getString("pesoIngresado");
        showInfo();

        refreshData();
        return view;
    }




    private void showInfo(){
        double a = 0;
        double b = 0;
        double c = 0;
        double bajado = 0;
        double perdidos = 0;

        TextView txInfoPesateHead = (TextView) view.findViewById(R.id.txInfoPesateHead);
        txInfoPesateHead.setText(registerWeigth + " kg");

        TextView txInfoPesate = (TextView) view.findViewById(R.id.txInfoPesate);
        txInfoPesate.setText(peso_total_comunidad);

        if(!registerWeigth.equals("") && !ultimoPeso.equals("") && !weigth_actual.equals("")){
             a = Double.parseDouble(registerWeigth);
             b = Double.parseDouble(ultimoPeso);
             c = Double.parseDouble(weigth_actual);
             bajado = b-a;
             perdidos = c-a;
             bajado = Math.round(bajado*100)/100.00;
             perdidos = Math.round(bajado*100)/100.00;
        }

        //93    70
        if(bajado >= 0){
            TextView bajado_subido =(TextView) view.findViewById(R.id.bajado_subido);
            bajado_subido.setText("Has bajado ");

            TextView pBajado = (TextView) view.findViewById(R.id.txBajado);
            pBajado.setText(String.valueOf(bajado)+ "kg ");
        }


        else if(bajado < 0){
            TextView bajado_subido =(TextView) view.findViewById(R.id.bajado_subido);
            bajado_subido.setText("Has subido ");

            bajado = bajado * -1;
            //perdidos = perdidos * -1;
            TextView pBajado = (TextView) view.findViewById(R.id.txBajado);
            pBajado.setText(String.valueOf(bajado)+ "kg ");

        }

        if(perdidos >=0){
            TextView perdidos_ganados = (TextView) view.findViewById(R.id.perdidos_ganados);
            perdidos_ganados.setText(" perdido(s)");

            TextView ptotal = (TextView) view.findViewById(R.id.totalDe);
            ptotal.setText(" " + String.valueOf(perdidos)+ "kg ");
        }

        else if(perdidos < 0){
            TextView perdidos_ganados = (TextView) view.findViewById(R.id.perdidos_ganados);
            perdidos_ganados.setText(" ganado(s)");

            perdidos = perdidos * -1;
            TextView ptotal = (TextView) view.findViewById(R.id.totalDe);
            ptotal.setText(String.valueOf(perdidos)+ "kg ");
        }

    }



    private void refreshData(){

            new Servicios(getContext()).getInfoInicio(Arrays.asList(
                    idcat
                    ),
                    new Servicios.DataCallback() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {
                                inforSatrt = GlobalVars.infoStart;
                                setData();

                            } else {
                                Toast.makeText(getApplicationContext(), "Error de conexión al actualizar información", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
    }




   private void setData(){
           double x1 = 0;
           double x2 = 0;
           JSONObject obj = null;
           try {
               obj = new JSONObject(inforSatrt.toString());
               peso_total = obj.getString("peso_total");
               dieta = obj.getString("dieta");
               JSONObject result = obj.getJSONObject("ultimo_peso");
               diasHace = result.getString("dias");
               ultimoPesoD = Double.parseDouble(result.getString("peso"));
               proxima_consulta = obj.getString("proxima_consulta");


               //if(obj.getBoolean("user_peso") != false){
               JSONObject user_peso = obj.getJSONObject("user_peso");
               if (!user_peso.getString("pesoActual").equals("") && !user_peso.getString("pesoDeseado").equals("")) {
                   pesoActual = Double.parseDouble(user_peso.getString("pesoActual"));
                   pesoDeseado = Double.parseDouble(user_peso.getString("pesoDeseado"));

                   if(ultimoPesoD == 0){
                       ultimoPesoD = pesoActual;
                   }
                   //ACT - DES = X1  ,  ULT - DES = X2  ,  AVANCE = ? = X2 * 100 / X1
                   x1 = pesoActual - pesoDeseado;
                   x2 = ultimoPesoD - pesoDeseado;
                   avance = ((x1 - x2) * 100) / (x1);
               }
               //}


               dias = obj.getString("dias");
               proxima_consulta = obj.getString("proxima_consulta");
               sharedPreferences = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
               sharedPreferences.edit()
                       .putString("proxima_consulta", proxima_consulta)
                       .putString("last_weigth", String.valueOf(ultimoPesoD))
                       .putString("advance", String.valueOf(avance))
                       .putString("last_weigth_days", String.valueOf(diasHace))
                       .putString("dias", String.valueOf(dias))

                       .putString("iddieta", String.valueOf(dieta))
                       .putString("weigth_actual", String.valueOf(pesoActual))
                       .putString("peso_total_comunidad", String.valueOf(peso_total))
                       .putString("pesoDeseado", String.valueOf(pesoDeseado))
                       .apply();
           } catch (JSONException e) {
               e.printStackTrace();
           }

    }





}
