package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;


public class  FMainConsulta extends Fragment {

    private View view;
    private Spinner spUno, spDos, spTres, spCuatro, spCinc, spSeis, spSiet, spOcho, spNuev, spDiez, spOnce, spDoce, spTrece, spCatorce, spQuince, spDieSeis;
    private int proxima_consulta;
    private String proxima_consultaS;
    private String[] answers = new String[18];
    private Button btnConsultaSemanal;
    private
    String idcat = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        SharedPreferences varProxConsulta = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        proxima_consultaS = varProxConsulta.getString("proxima_consulta", "");
        idcat = varProxConsulta.getString("idcat", "");
        if (!proxima_consultaS.equals("")) {
            proxima_consulta = Integer.valueOf(proxima_consultaS);
        }

        if (proxima_consulta <= 0) {
            view = inflater.inflate(R.layout.navigation_fconsulta_semanal, container, false);
            fillSpinners();
            metodoButton();
        } else {
            view = inflater.inflate(R.layout.navigation_fmain_consulta, container, false);
            TextView txhoras = (TextView) view.findViewById(R.id.txHoras);
            if (proxima_consulta <= 24) {
                txhoras.setText(proxima_consultaS + "\nHoras");
            } else {
                int i = proxima_consulta / 24;
                txhoras.setText(i + "\nDía(s)");
            }
        }


        return view;
    }

    private void metodoButton() {
        btnConsultaSemanal = (Button) view.findViewById(R.id.btnConsultaSemanal);

        btnConsultaSemanal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAnswersWeek();
                validateAnswers();
                if (validateAnswers()) {
                    saveAnswers();
                } else {
                    Toast.makeText(getContext(), "Cuestionario Incompleto\nFavor de llenar todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void fillSpinners() {


        spUno = (Spinner) view.findViewById(R.id.spinn_sem_uno);
        spDos = (Spinner) view.findViewById(R.id.spinn_sem_dos);
        spTres = (Spinner) view.findViewById(R.id.spinn_sem_tres);
        spCuatro = (Spinner) view.findViewById(R.id.spinn_sem_cua);
        spCinc = (Spinner) view.findViewById(R.id.spinn_sem_cin);
        spSeis = (Spinner) view.findViewById(R.id.spinn_sem_sei);
        spSiet = (Spinner) view.findViewById(R.id.spinn_sem_sie);
        spOcho = (Spinner) view.findViewById(R.id.spinn_sem_och);
        spNuev = (Spinner) view.findViewById(R.id.spinn_sem_nue);
        spDiez = (Spinner) view.findViewById(R.id.spinn_sem_die);
        spOnce = (Spinner) view.findViewById(R.id.spinn_sem_onc);
        spDoce = (Spinner) view.findViewById(R.id.spinn_sem_doc);
        spTrece = (Spinner) view.findViewById(R.id.spinn_sem_trec);
        spCatorce = (Spinner) view.findViewById(R.id.spinn_sem_cat);
        spQuince = (Spinner) view.findViewById(R.id.spinn_sem_quin);
        spDieSeis = (Spinner) view.findViewById(R.id.spinn_sem_dysei);

        String[] mes = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String fecha = new StringBuilder()
                .append(calendar.get(Calendar.DAY_OF_MONTH)).append(" de ")
                .append(mes[calendar.get(Calendar.MONTH)]).append(" de ")
                .append(calendar.get(Calendar.YEAR))
                .toString();
        TextView txFecha = (TextView) view.findViewById(R.id.txFecha);
        txFecha.setText(fecha);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Selecciona una opción");
        categories.add("Sí");
        categories.add("No pude");
        categories.add("En lo posible");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUno.setAdapter(dataAdapter);


        List<String> actFis = new ArrayList<String>();
        actFis.add("Selecciona una opción");
        actFis.add("Mucho");
        actFis.add("Regular");
        actFis.add("No");
        actFis.add("No me importa que sea de mi gusto");
        ArrayAdapter<String> dAactFisic = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, actFis);
        dAactFisic.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDos.setAdapter(dAactFisic);

        List<String> lTres = new ArrayList<String>();
        lTres.add("Selecciona una opción");
        lTres.add("Sí");
        lTres.add("5 veces por semana");
        lTres.add("Estoy estreñido");
        lTres.add("Me solté");
        ArrayAdapter<String> daTres = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lTres);
        daTres.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTres.setAdapter(daTres);

        List<String> lCuatro = new ArrayList<String>();
        lCuatro.add("Selecciona una opción");
        lCuatro.add("Igual que siempre");
        lCuatro.add("Sí");
        lCuatro.add("Notoriamente más");
        ArrayAdapter<String> daCuatro = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lCuatro);
        daCuatro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCuatro.setAdapter(daCuatro);

        List<String> lcinco = new ArrayList<String>();
        lcinco.add("Selecciona una opción");
        lcinco.add("Sí");
        lcinco.add("Lo normal en mí");
        lcinco.add("Sí, excepto la primera");
        ArrayAdapter<String> daCinco = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lcinco);
        daCinco.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCinc.setAdapter(daCinco);

        List<String> lseis = new ArrayList<String>();
        lseis.add("Selecciona una opción");
        lseis.add("Sí");
        lseis.add("No");
        lseis.add("A veces");
        ArrayAdapter<String> daSeis = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lseis);
        daSeis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSeis.setAdapter(daSeis);

        List<String> lSiete = new ArrayList<String>();
        lSiete.add("Selecciona una opción");
        lSiete.add("Sí");
        lSiete.add("Solo por las mañanas");
        lSiete.add("No");
        lSiete.add("No puse atención");
        ArrayAdapter<String> daSiete = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lSiete);
        daSiete.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSiet.setAdapter(daSiete);

        List<String> lOcho = new ArrayList<String>();
        lOcho.add("Selecciona una opción");
        lOcho.add("No");
        lOcho.add("Me siento mejor que antes");
        lOcho.add("Sí");
        ArrayAdapter<String> daOcho = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lOcho);
        daOcho.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOcho.setAdapter(daOcho);

        List<String> lNueve = new ArrayList<String>();
        lNueve.add("Selecciona una opción");
        lNueve.add("Un poco");
        lNueve.add("Significativo");
        lNueve.add("No sé");
        lNueve.add("No");
        ArrayAdapter<String> daNueve = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lNueve);
        daNueve.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spNuev.setAdapter(daNueve);

        List<String> lDiez = new ArrayList<String>();
        lDiez.add("Selecciona una opción");
        lDiez.add("No");
        lDiez.add("Sí");
        lDiez.add("Sorprendente");
        ArrayAdapter<String> daDiez = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lDiez);
        daDiez.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDiez.setAdapter(daDiez);

        List<String> lOnce = new ArrayList<String>();
        lOnce.add("Selecciona una opción");
        lOnce.add("Sí");
        lOnce.add("No");
        lOnce.add("No sé");
        ArrayAdapter<String> daOnce = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lOnce);
        daOnce.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spOnce.setAdapter(daOnce);

        List<String> lDoce = new ArrayList<String>();
        lDoce.add("Selecciona una opción");
        lDoce.add("Sí");
        lDoce.add("No");
        ArrayAdapter<String> daDoce = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lDoce);
        daDoce.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDoce.setAdapter(daDoce);

        List<String> lTrece = new ArrayList<String>();
        lTrece.add("Selecciona una opción");
        lTrece.add("Sí");
        lTrece.add("Aún no");
        ArrayAdapter<String> daTrece = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lTrece);
        daTrece.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTrece.setAdapter(daTrece);

        List<String> lCatroce = new ArrayList<String>();
        lCatroce.add("Selecciona una opción");
        lCatroce.add("Sí y dejo de comer");
        lCatroce.add("Sí y continúo comiendo");
        lCatroce.add("No");
        ArrayAdapter<String> daCatroce = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lCatroce);
        daCatroce.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCatorce.setAdapter(daCatroce);

        List<String> lQuince = new ArrayList<String>();
        lQuince.add("Selecciona una opción");
        lQuince.add("Sí");
        lQuince.add("Aún no");
        lQuince.add("Mucho menos");
        ArrayAdapter<String> daQuince = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lQuince);
        daQuince.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spQuince.setAdapter(daQuince);

        List<String> lDSeis = new ArrayList<String>();
        lDSeis.add("Selecciona una opción");
        lDSeis.add("Sí");
        lDSeis.add("A veces");
        lDSeis.add("Aún no");
        ArrayAdapter<String> daDSeis = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, lDSeis);
        daDSeis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDieSeis.setAdapter(daDSeis);

    }


    private void getAnswersWeek() {

        EditText edtCualesMalestar = (EditText) view.findViewById(R.id.edtOcho);

        answers[0] = String.valueOf((spUno.getSelectedItemPosition()));
        answers[1] = String.valueOf((spDos.getSelectedItemPosition()));
        answers[2] = String.valueOf((spTres.getSelectedItemPosition()));
        answers[3] = String.valueOf((spCuatro.getSelectedItemPosition()));
        answers[4] = String.valueOf((spCinc.getSelectedItemPosition()));
        answers[5] = String.valueOf((spSeis.getSelectedItemPosition()));
        answers[6] = String.valueOf((spSiet.getSelectedItemPosition()));
        answers[7] = String.valueOf((spOcho.getSelectedItemPosition()));
        answers[8] = edtCualesMalestar.getText().toString();
        answers[9] = String.valueOf((spNuev.getSelectedItemPosition()));
        answers[10] = String.valueOf((spDiez.getSelectedItemPosition()));
        answers[11] = String.valueOf((spOnce.getSelectedItemPosition()));
        answers[12] = String.valueOf((spDoce.getSelectedItemPosition()));
        answers[13] = String.valueOf((spTrece.getSelectedItemPosition()));
        answers[14] = String.valueOf((spCatorce.getSelectedItemPosition()));
        answers[15] = String.valueOf((spQuince.getSelectedItemPosition()));
        answers[16] = String.valueOf((spDieSeis.getSelectedItemPosition()));
        answers[17] = idcat;

    }


    private void saveAnswers() {


        new Servicios(getContext()).cuestionarioWeek(Arrays.asList(
                answers[0],
                answers[1],
                answers[2],
                answers[3],
                answers[4],
                answers[5],
                answers[6],
                answers[7],
                answers[8],
                answers[9],
                answers[10],
                answers[11],
                answers[12],
                answers[13],
                answers[14],
                answers[15],
                answers[16],
                answers[17]
                ),

                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {

                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.flInicio, new FWelcome(), "FWelcome");
                            ft.commit();
                            Toast.makeText(getContext(), "Cuestionario guardado correctamente", Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }


    public boolean validateAnswers() {
        for (int i = 0; i <= 16; i++) {
            if (i != 8 && answers[i].equals("0")) {
                return false;
            }
            //validate when selected true the question 7 then should fill question malestar
            else if ((i == 8) && answers[i].equals("") && answers[7] == "3") {
                return false;
            }
        }
        return true;
    }


}
