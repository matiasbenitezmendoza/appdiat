package com.diat.app.navigation;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.diat.app.webservices.Servicios;

/**
 * Created by webandando on 25/11/16.
 */

public class FAgendar extends Fragment implements View.OnClickListener {

    private View view;
    private TimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private TextView tvDate;
    private TextView tvTime;
    private Button btnAgendar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigation_frag_fagendar, container, false);
        setCasting();
        return view;
    }

    private void setCasting() {
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        tvTime = (TextView) view.findViewById(R.id.tvTime);
        tvDate.setOnClickListener(this);
        tvTime.setOnClickListener(this);
        btnAgendar = (Button) view.findViewById(R.id.btnAgendar);
        btnAgendar.setOnClickListener(this);
        Calendar c = Calendar.getInstance(Locale.getDefault());
        timePickerDialog = new TimePickerDialog(
                getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        tvTime.setText(String.format(Locale.getDefault(), "%02d", hourOfDay) + ":" + String.format(Locale.getDefault(), "%02d", minute) + ":00");
                    }
                },
                12,
                0,
                true
        );

        datePickerDialog = new DatePickerDialog(
                getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tvDate.setText( String.format(Locale.getDefault(), "%02d", dayOfMonth) + "-" + String.format(Locale.getDefault(), "%02d", month + 1) + "-" + String.format(Locale.getDefault(), "%02d", year) );
                    }
                },
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        );

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTime:
                timePickerDialog.show();
                break;
            case R.id.tvDate:
                datePickerDialog.show();
                break;
            case R.id.btnAgendar:
                if (isValidFormat("dd-MM-yyyy", tvDate.getText().toString()) && isValidFormat("HH:mm:ss", tvTime.getText().toString())){
                    agendarCita();
                }else{
                    Toast.makeText(getContext(), "Elija fecha y hora.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void agendarCita(){
        new Servicios(getContext()).agendar(
                Arrays.asList(
                        getActivity().getSharedPreferences("PreferencesRegisto", Context.MODE_PRIVATE).getString("idcat", ""),
                        tvDate.getText().toString(),
                        tvTime.getText().toString()
                ),
                new Servicios.DataCallback.Extra() {
                    @Override
                    public void onResult(boolean result, String extra) {
                        if (result){
                            Toast.makeText(getContext(), "Se ha enviado la informaicón correctamente", Toast.LENGTH_LONG).show();
                            ((LandingActivityB) getActivity()).inicio_welcome(null);
                        } else {
                            Toast.makeText(getContext(), extra, Toast.LENGTH_LONG).show();
                        }
                    }
                }

        );
    }

    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }
}
