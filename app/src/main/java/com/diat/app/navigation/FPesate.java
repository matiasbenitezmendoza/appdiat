package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;


public class FPesate extends Fragment {

    private View view;
    private String last_weigth = "";
    private Button btnRegistraPeso;
    private EditText edtPesoIngresado;
    private String idusuario;
    private String mensaj = "";
    private Double pesoMin = 0.0;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.navigation_frag_fpesate, container, false);



        SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        last_weigth = userDet.getString("last_weigth", "");
        idusuario = userDet.getString("idcat", "");

        TextView edtLastWe = (TextView) view.findViewById(R.id.txPesoPesate);
        edtLastWe.setText(last_weigth + " kg");

        registraPeso();
        return view;
    }







  private void registraPeso(){


      edtPesoIngresado = (EditText) view.findViewById(R.id.edtPesoIngresado);
      btnRegistraPeso = (Button) view.findViewById(R.id.btnRegistraPeso);
      btnRegistraPeso.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {


              if(!edtPesoIngresado.getText().toString().equals("")) {
                   pesoMin = Double.parseDouble(edtPesoIngresado.getText().toString());
              }


              if(!edtPesoIngresado.getText().toString().equals("") && (pesoMin) > 40 && (pesoMin) < 220) {


                  new Servicios(getContext()).weigthRegister(Arrays.asList(
                          idusuario.toString(),
                          edtPesoIngresado.getText().toString()
                          ),

                          new Servicios.DataCallback() {
                              @Override
                              public void onResult(boolean result) {
                                  if (result){
                                      /*SharedPreferences userW = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                      SharedPreferences.Editor edit = userW.edit();
                                      userW.edit().putString("last_weigth",  edtPesoIngresado.getText().toString()).apply();
                                      edit.commit();*/

                                      FInfoPesate fInfoPesate = new FInfoPesate();
                                      Bundle args = new Bundle();
                                      args.putString("pesoIngresado", edtPesoIngresado.getText().toString());
                                      fInfoPesate.setArguments(args);

                                      FragmentTransaction ft = getFragmentManager().beginTransaction();
                                      ft.replace(R.id.flInicio, fInfoPesate, "FInfoPesate");
                                      ft.commit();
                                  }

                                  else {//REDIRECT TO FRAGMENT FPesate
                                      final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                      ft.replace(R.id.flInicio, new FPesate(), "FPesate");
                                      ft.commit();
                                  }


                              }
                          });
              }
              else{
                  if(!edtPesoIngresado.getText().toString().equals("") && pesoMin < 40){
                      mensaj = "Ingrese un valor correcto";
                  }
                  else if(!edtPesoIngresado.getText().toString().equals("") &&  pesoMin > 220){
                      mensaj = "Ingrese un valor correcto";
                  }
                  else{ mensaj = "Ingrese su peso";}
                  Toast.makeText(getContext(), mensaj, Toast.LENGTH_SHORT).show();
              }
          }
      });
  }
}
