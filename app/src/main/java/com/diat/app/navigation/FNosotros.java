package com.diat.app.navigation;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diat.app.diat.R;


public class FNosotros extends Fragment {

    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_fnosotros, container, false);

        justifyTexts();


        return view;
    }

    private void justifyTexts() {
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.light.ttf");

        TextView tx_nosotros_intro = (TextView) view.findViewById(R.id.tx_nosotros_intro);
        tx_nosotros_intro.setTypeface(type);

        TextView tx_fundadoraU = (TextView) view.findViewById(R.id.tx_fundadoraU);
        tx_fundadoraU.setTypeface(type);

        TextView tx_fundadoraD = (TextView) view.findViewById(R.id.tx_fundadoraD);
        tx_fundadoraD.setTypeface(type);
    }

}
