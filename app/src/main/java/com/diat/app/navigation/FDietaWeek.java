package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;


public class  FDietaWeek extends Fragment {

    private View view;
    private String iddieta, dataDiet, nombre;
    private Button btnRecomendaciones, btnMenu;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_fdieta_week, container, false);

        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        iddieta  = varReferenceCargo.getString("iddieta", "");

        if(iddieta.equals("0")) {
            TextView txInicioSemana = (TextView) view.findViewById(R.id.txInicioSemana);
            txInicioSemana.setText("No existe dieta asociada al usuario!");
        }
        else{
            fillInfoStart();
            recomendaciones();
            menu();
        }

         return view;
    }

    private void fillInfoStart() {

            new Servicios(getContext()).getDataDiet(Arrays.asList(
                    iddieta
                    ),

                    new Servicios.DataCallback() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {

                                dataDiet = GlobalVars.dataDiet;

                                fillFields();
                                //Toast.makeText(getApplicationContext(), "Datos ok: ", Toast.LENGTH_SHORT).show();
                            }
                            else{


                            }
                        }
                    });

    }





   private void fillFields() {

       nombre = "";
       String descripcion = "";
        try {
            JSONObject obj = new JSONObject(dataDiet);
            JSONObject result = obj.getJSONObject("dieta");
            JSONObject nom = result.getJSONObject("info");



            nombre = nom.getString("nombre");
            descripcion = nom.getString("descripcion");


            TextView txTitle = (TextView) view.findViewById(R.id.txTitle);
            txTitle.setText(nombre);

            TextView txInicioSemana = (TextView) view.findViewById(R.id.txInicioSemana);
            txInicioSemana.setText(descripcion);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
   }





    private void recomendaciones() {

        btnRecomendaciones = (Button) view.findViewById(R.id.btnReco);
        btnRecomendaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FRecomendaciones fRecomendaciones = new FRecomendaciones();
                Bundle args = new Bundle();
                args.putString("titleRecomendaciones", nombre);
                fRecomendaciones.setArguments(args);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.flInicio, fRecomendaciones, "FRecomendaciones");
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }



    private void menu() {

        btnMenu = (Button) view.findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FMenu Fmenu = new FMenu();
                Bundle args = new Bundle();
                args.putString("titleRecomendaciones", nombre);
                Fmenu.setArguments(args);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.flInicio, Fmenu, "FMenu");
                ft.addToBackStack(null);
                ft.commit();
            }
        });
    }



}
