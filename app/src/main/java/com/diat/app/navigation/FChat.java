package com.diat.app.navigation;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diat.app.diat.R;

import com.diat.app.webservices.Servicios;

/**
 * Created by webandando on 23/11/16.
 */

public class FChat extends Fragment implements View.OnClickListener {

    private SharedPreferences sharedPreferences;
    private View view;
    private LinearLayout llCita;
    private LinearLayout llChat;
    private TextView tvCitas;
    private TextView llText;
    private ImageView llIcon;
    private FragmentManager fragmentManager;
    private String plan;
    private int casoCita;
    private final int PERMISSIONS_REQUEST = 500;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigation_frag_fchat, container, false);
        sharedPreferences = getActivity().getSharedPreferences("PreferencesRegisto", Context.MODE_PRIVATE);
        fragmentManager = getActivity().getSupportFragmentManager();
        setCasting();
        setButton();
        return view;
    }

    private void setButton() {
        if (plan.equals("VIP") || plan.equals("Audaz")) {
            new Servicios(getActivity().getApplicationContext()).citas(
                    sharedPreferences.getString("idcat", ""),
                    new Servicios.DataCallback.ExtraValue() {
                        @Override
                        public void onResult(boolean result, String extra, int caso) {
                            if (result) {
                                casoCita = caso;
                                switch (caso) {
                                    case 0:
                                        llText.setText("AGENDAR");
                                        llCita.setEnabled(true);
                                        break;
                                    case 1:
                                        llText.setText("PENDIENTE DE APROBACIÓN");
                                        llCita.setEnabled(false);
                                        break;
                                    case 2:
                                        llText.setText(extra);
                                        llCita.setEnabled(false);
                                        break;
                                    case 3:
                                        llText.setText("EMPIEZA TU CONSULTA");
                                        llCita.setEnabled(true);
                                        break;
                                    case 4:
                                        llText.setText("AGENDAR");
                                        llCita.setEnabled(true);
                                        break;
                                    default:
                                        break;
                                }

                            }
                        }
                    });
        }
    }

    private void setCasting() {
        plan = sharedPreferences.getString("tipoPlan", "");
        Log.e("respuesta", sharedPreferences.getAll().toString());
        tvCitas = (TextView) view.findViewById(R.id.tvCitas);
        llCita = (LinearLayout) view.findViewById(R.id.llCita);
        llCita.setOnClickListener(this);
        llChat = (LinearLayout) view.findViewById(R.id.llChat);
        llChat.setOnClickListener(this);
        llIcon = (ImageView) view.findViewById(R.id.llIcon);
        llText = (TextView) view.findViewById(R.id.llText);

        switch (plan) {
            case "VIP":
                tvCitas.setText("1. Fija el horario conveniente.\n2. Experimenta virtualmente tu cita: tal como si estuvieras ahí.\n3. Obtén tu dieta personalizada.\n4. Ahorra el tiempo por desplazo, evítate el tráfico y la espera. Reduce el estrés.\n5. Paga, incluso en esta modalidad, menos de la mitad que una consulta presencial.");
                llCita.setBackground(getResources().getDrawable(R.drawable.selector_btn_skype));
                llIcon.setImageDrawable(getResources().getDrawable(R.drawable.ico_skype));
                llCita.setVisibility(View.VISIBLE);
                llCita.setOnClickListener(this);
                break;
            case "Audaz":
                tvCitas.setText("1. Acuerda una cita en el horario disponible de tu conveniencia.\n2. Obtén tu consulta privada a través de una llamada desde tu celular.\n3. Finaliza la consulta en aproximadamente 15 minutos.\n4. Paga menos de la cuarta parte de lo equivalente a una consulta presencial.");
                llCita.setBackground(getResources().getDrawable(R.drawable.selector_btn_verde));
                llIcon.setImageDrawable(getResources().getDrawable(R.drawable.ico_phone));
                llCita.setVisibility(View.VISIBLE);
                llCita.setOnClickListener(this);
                break;
            default:
                tvCitas.setText("a) Las preguntas que se te envían son las mismas que se te harían en las entrevistas personales y privadas, están diseñadas con todo detalle. Responde concienzudamente.\nb) Las dietas están explicadas de forma sencilla y clara para ti. Las encontrarás de dos formas: indicaciones prácticas o menús.\nc) Para dudas, contáctanos por nuestro exclusivo chat Diät. Como la consulta es personalizada, no esperes respuestas inmediatas.");
                llCita.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCita:
            switch (casoCita) {
                case 0: case 4:
                    fragmentManager.beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .replace(R.id.flInicio, new FAgendar(), "toFAgendar")
                            .commit();
                    break;
                case 3:
                    if (plan.equals("VIP")) {
                        skype();
                    } else {
                        permisos();
                    }
                    break;
                default:
                    break;
            }
            break;
            case R.id.llChat:
                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.flInicio, new FConversacion(), "toFConversacion")
                        .commit();
                break;
        }
    }

    private void permisos() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                getActivity().requestPermissions(
                        new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST);
            } else
                llamada();
        } else
            llamada();

    }

    private void llamada() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:52902626"));
        startActivity(intent);
    }


    public void skype() {
        try {
            Intent sky = new Intent("android.intent.action.VIEW");
            sky.setData(Uri.parse("skype:Diatapp?call"));
            startActivity(sky);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.skype.raider")));
        }

    }


}
