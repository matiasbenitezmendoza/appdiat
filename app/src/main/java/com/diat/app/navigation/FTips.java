package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.FragmentInicio;
import com.diat.app.diat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;

public class FTips extends Fragment {
    private View view;
    private ListView listView;
    //List<Map<String,String>> tipsList = new ArrayList<Map<String,String>>();
    String[] tipsArray;
    String tipsS;

    public FTips() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_tips, container, false);
        //listView = (ListView) view.findViewById(R.id.listTips);


        sendRequest();

        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        String isLogedUser = varReferenceCargo.getString("isLoged", "");

        if (isLogedUser == "1") {
            resizeLayTips();
        }


        return view;
    }


    private void sendRequest() {

        String iduser = "0";

        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        iduser = varReferenceCargo.getString("idcat", "");


        new Servicios(getContext()).getTips(Arrays.asList(
                iduser
                ),

                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {
                            tipsS = GlobalVars.r;
                            fillListTips();
                        } else {
                            //Toast.makeText(getApplicationContext(), "Error de conexión", Toast.LENGTH_SHORT).show();
                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.replace(R.id.flInicio, new FragmentInicio(), "FInicio");
                            //ft.addToBackStack(null);IF EL USER IS REGISTERED NO REGRESE A LA PANTALLA DE TIPO PLAN
                            ft.commit();

                            //MOSTRAR LA HAMBURGUER
                            ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
                            toolC.setVisibility(View.VISIBLE);

                        }
                    }
                });
    }


    private void initList() {

        try {
            JSONObject jsonResponse = new JSONObject(tipsS);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("tips");

            if (jsonMainNode != null) {

                tipsArray = new String[jsonMainNode.length()];

                for (int i = 0; i < jsonMainNode.length(); i++) {
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                    //String idtip = jsonChildNode.optString("idtip");
                    String tip = jsonChildNode.optString("tip");
                    String outPut = tip;
                    //tipsList.add(createEmployee("tips", outPut));

                    tipsArray[i] = outPut;
                }

            }
        } catch (JSONException e) {
            Toast.makeText(getContext(), "Error" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    private void fillListTips() {
        initList();
        listView = (ListView) view.findViewById(R.id.listTips);
        if (tipsArray != null) {

            //ListAdapter adaptador1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, tipsArray){
            ListAdapter adaptador1 = new ArrayAdapter<String>(getContext(), R.layout.simple_li_tips, tipsArray) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    if (position % 2 == 1) {
                        view.setBackgroundResource(R.color.gris_diat);
                    } else {
                        view.setBackgroundResource(R.color.gris_transparent);
                    }
                    return view;
                }
            };
            listView.setAdapter(adaptador1);

        }
    }


    private void resizeLayTips() {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.llTipss);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(8, 5, 8, 40);
        //(leftMargin, topMargin, rightMargin, bottomMargin);
        layout.setLayoutParams(params);

    }


}
