package com.diat.app.navigation;


import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.diat.app.diat.R;

import static android.content.Context.MODE_PRIVATE;


public class FTrayectoria extends Fragment {

    private View view;
    //private String last_weig;
    private String avance, dias;
    private String last_weigth_days;
    private Button btnGoToChart;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            view = inflater.inflate(R.layout.navigation_frag_ftrayectoria, container, false);

            fillData();
            buttonChart();

            return view;
        }



    private  void fillData(){

        double av = 0;
        int avan = 0;
        int last_weigth_daysI = 0;
        int daysI = 0;
        int weekRestantes = 0;
        int semanas = 0;


        SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        //last_weig = userDet.getString("last_weigth", "");
        avance = userDet.getString("advance", "");
        last_weigth_days = userDet.getString("last_weigth_days", "");
        dias = userDet.getString("dias", "");

        if(!avance.equals(""))
            {
             av = Double.parseDouble(avance);
             avan = ((int) av);
            }

        if(avan > 100){
            avan=100;
        }

        TextView txPercent = (TextView) view.findViewById(R.id.txTrayectoriaTwo);
        txPercent.setText(" "+ avan + "% ");




        if(avan >= 100){
            TextView txTrayectoriaFour = (TextView) view.findViewById(R.id.txTrayectoriaFour);
            txTrayectoriaFour.setText("Has logrado el ");
            TextView txTrayectoriaFive = (TextView) view.findViewById(R.id.txTrayectoriaFive);
            txTrayectoriaFive.setText(" 100%");
            TextView txTrayectoriaSix = (TextView) view.findViewById(R.id.txTrayectoriaSix);
            txTrayectoriaSix.setText("de tu meta en ");
        }

        else{
            TextView txTrayectoriaFive = (TextView) view.findViewById(R.id.txTrayectoriaFive);
            txTrayectoriaFive.setText(" 100%");
        }

        if(dias != ""){
        semanas = Integer.parseInt(dias)/7;}

        if(semanas == 0){
            TextView txSeman = (TextView) view.findViewById(R.id.txTrayectoriaTreeBis);
            //txSeman.setText(" " + last_weigth_days);
            txSeman.setText(dias);

            TextView txTrayectoriaTT = (TextView) view.findViewById(R.id.txTrayectoriaTT);
            txTrayectoriaTT.setText("día(s)");
           }

        else if(semanas != 0){
        TextView txSeman = (TextView) view.findViewById(R.id.txTrayectoriaTreeBis);
        //txSeman.setText(" " + last_weigth_days);
        txSeman.setText(" " + semanas);}




        ProgressBar pcircle = (ProgressBar) view.findViewById(R.id.circular);
        pcircle.setMax(100); // 100 maximum value for the progress value
        pcircle.setProgress(avan);
        pcircle.setSecondaryProgress(100);


        ObjectAnimator anim = ObjectAnimator.ofInt(pcircle, "progress", 0, avan);
        anim.setDuration(200);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        TextView txInfPerce = (TextView) view.findViewById(R.id.txInfPerce);
        txInfPerce.setText(avan+ "%");

        if(!dias.equals("") && !dias.equals("0.0"))
            {
                //para evitar error de division entre 0
                if(avan == 0){
                    avan = 1;
                }
                /*last_weigth_daysI = Integer.valueOf(last_weigth_days);
                weekRestantes = ((last_weigth_daysI * 100)/(avan))-last_weigth_daysI;*/
                last_weigth_daysI = Integer.valueOf(dias);
                weekRestantes = ((last_weigth_daysI * 100)/(avan))-last_weigth_daysI;

                if(weekRestantes == 0 ){

                   if(weekRestantes == 0 && avan < 100 ){//cuando semanas son 0 y aun no alcanza la meta
                       weekRestantes = 1; //se setea por default a 1 semana
                        String wRes = String.valueOf(weekRestantes);

                        TextView txSem = (TextView) view.findViewById(R.id.txSem);
                        txSem.setText(wRes);
                    }
                    else {//cuando ya alcanzo su meta
                        daysI = Integer.valueOf(dias);
                        weekRestantes = daysI / 7;

                       if(weekRestantes == 0){
                           String wRes = String.valueOf(daysI);
                           TextView txSem = (TextView) view.findViewById(R.id.txSem);
                           txSem.setText(wRes);
                           TextView txsemanas_dias = (TextView) view.findViewById(R.id.txsemanas_dias);
                           txsemanas_dias.setText("día(s)");
                       }
                       else{
                        String wRes = String.valueOf(weekRestantes);
                        TextView txSem = (TextView) view.findViewById(R.id.txSem);
                        txSem.setText(wRes);
                       }
                    }
                }
                else if(weekRestantes > 0 ){
                        if(weekRestantes < 7){
                            String wRes = String.valueOf(weekRestantes);
                            TextView txSem = (TextView) view.findViewById(R.id.txSem);
                            txSem.setText(wRes);

                            TextView txsemanas_dias = (TextView) view.findViewById(R.id.txsemanas_dias);
                            txsemanas_dias.setText("día(s)");
                        }
                        else{
                            weekRestantes = weekRestantes/7;

                            String wRes = String.valueOf(weekRestantes);
                            TextView txSem = (TextView) view.findViewById(R.id.txSem);
                            txSem.setText(wRes);}
                   }
                else if(weekRestantes < 0){
                    weekRestantes = weekRestantes/7;
                    weekRestantes = weekRestantes * -1;
                    String wRes = String.valueOf(weekRestantes);
                    TextView txSem = (TextView) view.findViewById(R.id.txSem);
                    txSem.setText(wRes);
                }


            }


    }



    public void buttonChart()
        {

        btnGoToChart = (Button) view.findViewById(R.id.btnGoToChart);
        btnGoToChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //startActivity(new Intent(getApplicationContext(), ChartActivity.class));

                 final FragmentTransaction ft = getFragmentManager().beginTransaction();
                 ft.replace(R.id.flInicio, new FChart(), "FChart");
                 ft.addToBackStack(null);
                 ft.commit();



            }
        });
     }





}
