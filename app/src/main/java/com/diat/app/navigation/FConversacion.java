package com.diat.app.navigation;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.diat.app.auxclases.ModelChat;
import com.diat.app.auxclases.firebase.FirebaseKeys;
import com.diat.app.auxclases.firebase.NotificationUtils;
import com.diat.app.diat.R;

import java.util.ArrayList;
import java.util.Arrays;

import com.diat.app.webservices.Servicios;

public class FConversacion extends Fragment {

    private View view;
    private ListView ltvChat;
    private EditText edtMensaje;
    private ImageButton btnMensaje;
    private Servicios servicios;
    private SharedPreferences sharedPreferences;
    private BroadcastReceiver broadcastReceiver;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigation_frag_fconversacion, container, false);
        casting();
        return view;
    }

    private void casting() {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(FirebaseKeys.PUSH_NOTIFICATION)) {
                    actualizar_chat();
                }
            }
        };
        ltvChat = (ListView) view.findViewById(R.id.ltvChat);
        edtMensaje = (EditText) view.findViewById(R.id.edtMensaje);
        btnMensaje = (ImageButton) view.findViewById(R.id.btnMensaje);
        servicios = new Servicios(getContext());
        sharedPreferences = getActivity().getSharedPreferences("PreferencesRegisto", Context.MODE_PRIVATE);
        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMensaje.getText().length() != 0) {
                    llenarChat("1", edtMensaje.getText().toString(), true);
                    edtMensaje.setText("");
                }
            }
        });
        llenarChat("0", "", true);
    }

    private void llenarChat(String guardar, String mensaje, boolean animar) {
        servicios.chat(Arrays.asList(sharedPreferences.getString("idcat", ""), guardar, mensaje),
                animar,
                new Servicios.DataCallback.ExtraAny() {
                    @Override
                    public void onResult(boolean result, ModelChat modelChat) {
                        MensajeAdapter adapter = new MensajeAdapter(modelChat.getChats());
                        ltvChat.setAdapter(adapter);
                    }
                });
    }

    public void actualizar_chat() {
        llenarChat("0", "", false);
        NotificationUtils.clearNotifications(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver,
                new IntentFilter(FirebaseKeys.PUSH_NOTIFICATION));
        NotificationUtils.clearNotifications(getContext());
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    private class MensajeAdapter extends BaseAdapter {

        private ArrayList<ModelChat.Chat> chats;
        private LayoutInflater inflater = null;

        MensajeAdapter(ArrayList<ModelChat.Chat> chats) {
            this.chats = chats;
            inflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return chats.size();
        }

        @Override
        public Object getItem(int position) {
            return chats.get(position);
        }

        @Override
        public long getItemId(int position) {
            return chats.get(position).getId();
        }

        @SuppressLint({"ViewHolder", "InflateParams"})
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi;
            Holder holder = new Holder();

            vi = inflater.inflate(R.layout.adapter_chat_item, null);

            if (chats.get(position).getIdusuario() == -1)
            vi.setBackgroundResource(R.drawable.shape_chat_rojo);
            else
                vi.setBackgroundResource(R.drawable.shape_chat_gris);

            holder.tvMensaje = (TextView) vi.findViewById(R.id.mensaje);
            holder.tvFecha = (TextView) vi.findViewById(R.id.fecha);

            holder.tvMensaje.setText(chats.get(position).getMensaje());
            holder.tvFecha.setText(chats.get(position).getFecha());
            return vi;
        }

        private class Holder {
            TextView tvMensaje;
            TextView tvFecha;
        }
    }
}
