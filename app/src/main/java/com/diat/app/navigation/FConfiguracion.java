package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.diat.app.diat.R;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CHARGE;
import static com.diat.app.auxclases.Constantes.KeysConfig.CONFIG;
import static com.diat.app.auxclases.Constantes.KeysConfig.NOTIFICATIONS;


public class FConfiguracion extends Fragment {

    private View view;
    private Switch swNotif;
    private Switch swCobro;
    private String notification;
    private String charge;
    private String valueNotifications, valueCharge, idcat;
    private TextView oswaldRegular2, txpag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.navigation_frag_fconfiguracion, container, false);
        swNotif = (Switch) view.findViewById(R.id.swNotifications);
        swCobro = (Switch) view.findViewById(R.id.swCobro);
        SharedPreferences varReferenceCargo = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        idcat = varReferenceCargo.getString("idcat", "");

        oswaldRegular2 = (TextView) view.findViewById(R.id.oswaldRegular2);
        txpag = (TextView) view.findViewById(R.id.txpag);



        setSwitchsState();
        changeSwNot();
        changeSwCob();

        justifyTexts();

        return view;
    }


    private void setSwitchsState() {

        SharedPreferences varReferenceCargo = getContext().getSharedPreferences(CONFIG, MODE_PRIVATE);
        notification = varReferenceCargo.getString(NOTIFICATIONS, "");
        charge = varReferenceCargo.getString(CHARGE, "");

        switch (notification) {
            case "0":
                swNotif.setChecked(false);
                valueNotifications = "0";
                oswaldRegular2.setText("Activar notificación de tip del día");
                break;
            case "1":
                swNotif.setChecked(true);
                valueNotifications = "1";
                oswaldRegular2.setText("Desactivar notificación de tip del día");
                break;
        }
        switch (charge) {
            case "0":
                swCobro.setChecked(false);
                valueCharge = "0";
                txpag.setText("Pago automático desactivado");
                break;
            case "1":
                swCobro.setChecked(true);
                valueCharge = "1";
                txpag.setText("Pago automático activado");
                break;
        }

    }


    private void changeSwNot() {
        swNotif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (swNotif.isChecked() == true) {
                    valueNotifications = "1";
                    oswaldRegular2.setText("Desactivar notificación de tip del día");

                } else {
                    valueNotifications = "0";
                    oswaldRegular2.setText("Activar notificación de tip del día");
                }

                if (swCobro.isChecked() == true) {
                    valueCharge = "1";
                    txpag.setText("Pago automático activado");
                } else {
                    valueCharge = "0";
                    txpag.setText("Pago automático desactivado");
                }

                sendStates();
            }
        });

    }


    private void changeSwCob() {
        swCobro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (swNotif.isChecked() == true) {
                    valueNotifications = "1";
                    oswaldRegular2.setText("Desactivar notificación de tip del día");
                } else {
                    valueNotifications = "0";
                    oswaldRegular2.setText("Activar notificación de tip del día");
                }

                if (swCobro.isChecked() == true) {
                    valueCharge = "1";
                    txpag.setText("Pago automático activado");
                } else {
                    valueCharge = "0";
                    txpag.setText("Pago automático desactivado");
                }
                sendStates();
            }
        });

    }


    private void sendStates() {

        new Servicios(getContext()).configNotifications(Arrays.asList(
                idcat,
                valueNotifications,
                valueCharge
                ),

                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {

                            SharedPreferences userConfig = getContext().getSharedPreferences(CONFIG, MODE_PRIVATE);
                            SharedPreferences.Editor edit = userConfig.edit();
                            userConfig.edit().putString(NOTIFICATIONS, valueNotifications).apply();
                            userConfig.edit().putString(CHARGE, valueCharge).apply();
                            edit.commit();

                        } else {
                        }


                    }
                });
    }


    private void justifyTexts() {
        Typeface type = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.light.ttf");

        TextView txContentConfig = (TextView) view.findViewById(R.id.txContentConfig);
        txContentConfig.setTypeface(type);

        TextView justifyTVPagoAut = (TextView) view.findViewById(R.id.justifyTVPagoAut);
        justifyTVPagoAut.setTypeface(type);
    }


}
