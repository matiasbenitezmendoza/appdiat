package com.diat.app.navigation;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;


import com.diat.app.auxclases.ExpandableListImageAd;
import com.diat.app.auxclases.GlobalVars;

import com.diat.app.diat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static com.diat.app.auxclases.Utilidades.firstLetterMayus;


public class FMenu extends Fragment {
    private View view;
    private String iddieta, dataDietMenu, idcat, dia;
    private List<String> listDataHeader, listDataHeaderTitles, listDataItems, listDataImages, listDataIds;
    //private ListViewImageAdapter adapter;
    private List<String> listDataBuffer;
    private List<String> listDataBufferImages;
    private List<String> listDataBufferIds;
    private HashMap<String, List<String>> listDataChild;
    private HashMap<String, List<String>> listDataChildImages;
    private HashMap<String, List<String>> listDataChildIds;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.navigation_frag_mmenu, container, false);
        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        iddieta = varReferenceCargo.getString("iddieta", "");
        idcat = varReferenceCargo.getString("idcat", "");

        //String[] dias={"0","1","2", "3","4","5","6"};
        Calendar cal = Calendar.getInstance();


        dia = String.valueOf(cal.get(Calendar.DAY_OF_WEEK));

        if (dia != "0") {
            int dy = Integer.parseInt(dia);
            dy = dy -1;
            dia = String.valueOf(dy);
        }
        //Toast.makeText(getContext(), "hoy es "+ dia , Toast.LENGTH_SHORT).show();


        fillList();


        return view;
    }


    private void fillList() {

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        dayOfTheWeek = firstLetterMayus(dayOfTheWeek);

        TextView tx_dia = (TextView) view.findViewById(R.id.tx_dia);
        tx_dia.setText(dayOfTheWeek);


        new Servicios(getContext()).getDataMenu(Arrays.asList(
                iddieta,
                dia,
                idcat
                ),

                new Servicios.DataCallback() {
                    @Override
                    public void onResult(boolean result) {
                        if (result) {

                            dataDietMenu = GlobalVars.dataDietMenu;
                            pushData();

                        } else {

                        }
                    }
                });
    }


    private void pushData() {
        ExpandableListImageAd listAdapter;
        ExpandableListView expListView;
        // get the listview
        expListView = (ExpandableListView) view.findViewById(R.id.elvMenu);

        // preparing list data
        parseData();

        listAdapter = new ExpandableListImageAd(getContext(), listDataHeaderTitles, listDataChild, listDataChildImages, listDataChildIds);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                FReceta Freceta = new FReceta();
                Bundle args = new Bundle();
                args.putString("idreceta", listDataChildIds.get(listDataHeaderTitles.get(groupPosition)).get(childPosition));
                Freceta.setArguments(args);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.flInicio, Freceta, "FReceta");
                ft.addToBackStack(null);
                ft.commit();
               /* Toast.makeText(
                        getApplicationContext(),
                                  listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(listDataHeaderTitles.get(groupPosition)).get(childPosition)
                                + listDataChildIds.get(listDataHeaderTitles.get(groupPosition)).get(childPosition),
                        Toast.LENGTH_SHORT)
                        .show();*/
                return false;
            }
        });


        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                /*if(groupPosition == 2) {
                    Toast.makeText(getContext() , "Group 2 clicked", Toast.LENGTH_LONG).show();
                    return true;
                } else*/
                return true;
            }
        });


        //to expand the headers group
        for (int i = 0; i < listDataHeader.size(); i++) {
            expListView.expandGroup(i);
        }


    }


    private void parseData() {

        listDataHeader = new ArrayList<String>();
        listDataHeaderTitles = new ArrayList<String>();
        //listDataImages = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataChildImages = new HashMap<String, List<String>>();
        listDataChildIds = new HashMap<String, List<String>>();

        //NODES FIXED IN JSON                 //TO SET TITLES IN THE HEADERS OF EXPANDABLE LV
        listDataHeader.add("DESAYUNO");
        listDataHeaderTitles.add("DESAYUNO");
        listDataHeader.add("SNACK_AM");
        listDataHeaderTitles.add("SNACK MATUTINO");
        listDataHeader.add("ENTRADA_COMIDA");
        listDataHeaderTitles.add("ENTRADA COMIDA");
        listDataHeader.add("MEDIO_COMIDA");
        listDataHeaderTitles.add("MEDIO COMIDA");
        listDataHeader.add("FUERTE_COMIDA");
        listDataHeaderTitles.add("FUERTE COMIDA");
        listDataHeader.add("SNACK_PM");
        listDataHeaderTitles.add("SNACK VESPERTINO");
        listDataHeader.add("ENTRADA_CENA");
        listDataHeaderTitles.add("ENTRADA CENA");
        listDataHeader.add("FUERTE_CENA");
        listDataHeaderTitles.add("FUERTE CENA");


        try {
            JSONObject jsonResponse = new JSONObject(dataDietMenu);
            JSONObject l = jsonResponse.getJSONObject("menu");


            for (int h = 0; h < listDataHeader.size(); h++) {
                listDataIds = new ArrayList<String>();
                listDataItems = new ArrayList<String>();
                listDataImages = new ArrayList<String>();
                JSONArray mainNode = l.getJSONArray(listDataHeader.get(h));

                for (int i = 0; i < mainNode.length(); i++) {
                    JSONObject jsonChildNode = mainNode.getJSONObject(i);
                    String idreceta = jsonChildNode.optString("idreceta");
                    String nombre = jsonChildNode.optString("nombre");
                    String imagen = jsonChildNode.optString("imagen");

                    listDataIds.add(idreceta);
                    listDataItems.add(nombre);
                    listDataImages.add(imagen);
                }

                listDataBufferIds = new ArrayList<String>();
                listDataBuffer = new ArrayList<String>();
                listDataBufferImages = new ArrayList<String>();

                for (int k = 0; k < listDataItems.size(); k++) {
                    listDataBufferIds.add(listDataIds.get(k));
                    listDataBuffer.add(listDataItems.get(k));
                    listDataBufferImages.add(listDataImages.get(k));
                }

                listDataChildIds.put(listDataHeaderTitles.get(h).toString(), listDataBufferIds);
                listDataChild.put(listDataHeaderTitles.get(h).toString(), listDataBuffer);
                listDataChildImages.put(listDataHeaderTitles.get(h).toString(), listDataBufferImages);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}


