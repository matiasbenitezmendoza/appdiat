package com.diat.app.navigation;


import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.diat.app.auxclases.GlobalVars;
import com.diat.app.diat.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;


import com.diat.app.auxclases.MyMarkerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FChart extends Fragment implements OnChartGestureListener, OnChartValueSelectedListener {

    private View view;
    private LineChart mChart;
    private TextView tvX, tvY;
    private String idUser;
    private String data;
    private String[] pesosArray;
    private String[] rangosArray;
    private String weigth_inicial;
    private String last_weigth;
    private String avance;
    float maxY = 0;
    private ObjectAnimator anim;
    private String pesoDeseado;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigation_frag_fchart, container, false);



        return view;
    }


    public void onResume(){
        super.onResume();
        idUser = "0";

        SharedPreferences varReferenceCargo = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        idUser  = varReferenceCargo.getString("idcat", "");
        weigth_inicial  = varReferenceCargo.getString("weigth_actual", "");
        last_weigth = varReferenceCargo.getString("last_weigth", "");
        avance = varReferenceCargo.getString("advance", "");
        pesoDeseado = varReferenceCargo.getString("pesoDeseado", "");

        if(last_weigth != ""){
            maxY = Float.parseFloat(weigth_inicial);
            maxY = maxY+10;
        }

        getDataChart();


        fillProgress1();
        fillProgress2();
    }


    private void fillProgress1(){

        ProgressBar pcircle = (ProgressBar) view.findViewById(R.id.circularChart1);
        pcircle.setMax(100); // 100 maximum value for the progress value

        if(maxY == 0) {
            pcircle.setProgress(0);
            pcircle.setSecondaryProgress(100);
            anim = ObjectAnimator.ofInt(pcircle, "progress", 0, 0);
            anim.setDuration(200);
        }
        else {
            pcircle.setProgress(100);
            pcircle.setSecondaryProgress(100);
            anim = ObjectAnimator.ofInt(pcircle, "progress", 0, 100);
            anim.setDuration(200);
        }

        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        //SET THE PESO INICIAL
        TextView txSem = (TextView) view.findViewById(R.id.txIni);
        txSem.setText(weigth_inicial +" kg");

    }



    private void fillProgress2(){

        double av = 0.0;
        int avan = 0;


        if(!avance.equals(""))
        {
            av = Double.parseDouble(avance);
            avan = ((int) av);
        }
        ProgressBar pcircle = (ProgressBar) view.findViewById(R.id.circularChart2);
        pcircle.setMax(100); // 100 maximum value for the progress value
        pcircle.setProgress(avan);
        pcircle.setSecondaryProgress(100);


        ObjectAnimator anim = ObjectAnimator.ofInt(pcircle, "progress", 0, avan);
        anim.setDuration(200);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.start();

        //SET THE PESO ACTUAL
        TextView txAct = (TextView) view.findViewById(R.id.txAct);
        txAct.setText(last_weigth + " kg");
    }








    private void fillChart(){


        mChart = (LineChart) view.findViewById(R.id.chart1);
        mChart.setOnChartGestureListener(this);
        mChart.setOnChartValueSelectedListener(this);
        mChart.setDrawGridBackground(false);

        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);



        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(getContext(), R.layout.navigation_custom_marker_view);
        mv.setChartView(mChart); // For bounds control
        mChart.setMarker(mv); // Set the marker to the chart

        // x-axis limit line
        LimitLine llXAxis = new LimitLine(1f, "Index 10");
        llXAxis.setLineWidth(2f);
        //llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextColor(Color.WHITE);
        //xAxis.enableGridDashedLine(10f, 10f, 0f);
        //xAxis.setValueFormatter(new MyCustomXAxisValueFormatter());
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(maxY);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTextColor(Color.WHITE);
        //leftAxis.setYOffset(20f);
        //leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mChart.getAxisRight().setEnabled(false);
        mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        //mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mChart.getViewPortHandler().setMaximumScaleX(2f);

        // add data
          int count = rangosArray.length;
          setData(count);

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);

        mChart.animateX(2500);
        //mChart.invalidate();

        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);



    }





    private void getDataChart() {


            new Servicios(getContext()).getDataChart(Arrays.asList(
                    idUser
                    ),

                    new Servicios.DataCallback() {
                        @Override
                        public void onResult(boolean result) {
                            if (result) {

                                data = GlobalVars.arrayDataChart;

                                initArrays();
                                if(maxY== 0 || pesosArray.length == 0 ){
                                   TextView no_data_cart = (TextView) view.findViewById(R.id.no_data_cart);
                                    no_data_cart.setVisibility(View.VISIBLE);

                                    com.github.mikephil.charting.charts.LineChart chart1 = (com.github.mikephil.charting.charts.LineChart) view.findViewById(R.id.chart1);
                                    chart1.setVisibility(View.INVISIBLE);

                                }
                                else {
                                    fillChart();
                                }
                            }
                            else{


                            }
                        }
                    });

        }





    private void initArrays(){

        try{
            JSONObject jsonResponse = new JSONObject(data);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("pesos");
            JSONArray jsonRangesNode = jsonResponse.optJSONArray("rangos");
            pesosArray = new String[jsonMainNode.length()];
            rangosArray = new String[jsonRangesNode.length()];

            for(int i = 0; i<jsonMainNode.length();i++){

                pesosArray[i] = jsonMainNode.getString(i);
                rangosArray[i] = jsonRangesNode.optString(i);

            }
        }
        catch(JSONException e){
            Toast.makeText(getApplicationContext(), "Error"+e.toString(), Toast.LENGTH_SHORT).show();
        }
    }





    private void setData(int count) {

        Float pesInicial = Float.valueOf(weigth_inicial);

        Float pDeseado = Float.valueOf(pesoDeseado);
        float valU = 0;
        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i = 0; i < count; i++) {
            if(i == 0){
                values.add(new Entry(i, pesInicial));
            }
            else{
             valU =  Float.parseFloat(pesosArray[i]);

            values.add(new Entry(i, valU));}
        }


        ArrayList<Entry> values2 = new ArrayList<Entry>();

        for (int i = 0; i < 1; i++) {

             float val = (float) (pDeseado);
            values2.add(new Entry(count+i, val));
        }

        LineDataSet set1;
        LineDataSet set2;



        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);

            set2 = (LineDataSet)mChart.getData().getDataSetByIndex(0);
            set2.setValues(values2);


            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set2 = new LineDataSet(values2, "");
            // set the line to be drawn like this "- - - - - -"
            //set1.enableDashedLine(10f, 5f, 0f);
            //set1.enableDashedHighlightLine(10f, 5f, 0f);
            //set1.setColor(R.color.rojo_diat);
            set1.setCircleColor(Color.rgb(0,235,235));
            set2.setColor(Color.rgb(4,242,254));
            set1.setLineWidth(1f);
            set1.setCircleRadius(4f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            //set1.setDrawFilled(true);
            set1.setFormLineWidth(0f);
            //set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);
            set1.disableDashedLine();



            set2.setCircleColor(Color.rgb(226,86,62));
            set2.setColor(Color.rgb(226,86,62));
            //set2.setLineWidth(1f);
            set2.setCircleRadius(4f);
            set2.setDrawCircleHole(false);
            set2.setValueTextSize(9f);
            set2.setFormLineWidth(0f);
            set2.setFormSize(15.f);


            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets
            dataSets.add(set2);


            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            data.setValueTextColor(Color.WHITE);


            // set data
            mChart.setData(data);


        }
    }


    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}



