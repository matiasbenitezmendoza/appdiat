package com.diat.app.registro;

import java.util.ArrayList;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.diat.app.diat.R;

public class GridViewImageAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Integer> listPhoto = new ArrayList<Integer>();

    /** Constructor de clase */
    public GridViewImageAdapter(Context c){
        mContext = c;
        //se cargan las miniaturas
        listPhoto.add(R.drawable.mastercard_3x);
        listPhoto.add(R.drawable.visa_3x);
        listPhoto.add(R.drawable.amex_3x);

    }

    @Override
    public int getCount() {
        return listPhoto.size();
    }

    @Override
    public Object getItem(int position) {
        return listPhoto.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewgroup) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource( listPhoto.get(position) );
        imageView.setScaleType( ImageView.ScaleType.CENTER_CROP );
        imageView.setLayoutParams( new GridView.LayoutParams(70, 70) );
        return imageView;
    }

}