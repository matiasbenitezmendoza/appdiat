package com.diat.app.registro;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static java.lang.Integer.parseInt;

public class FCuestionarioUno extends Fragment {

    private View view;
    String [] respCuesUno = new String[40];
    private Spinner spinner;
    private Button btnCuestionarioUno;
    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.registro_frag_cuestionariouno, container, false);

        sharedPreferences = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);

        /**if es un user add by admin dont show layout privacity, terminos ...*/
        if(!sharedPreferences.getString("idCompania", "").equals("") && sharedPreferences.getString("cuestionario", "").equals(""))
            {
            //nothing, not exists layout terminos, aviso ...
            }
            else{
            /*PARA HACER INVISIBLE EL FOOTER DE AVISOS DE PRIVACIDAD*/
                LinearLayout llTerm;
                llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
                llTerm.setVisibility(View.GONE);
            }

        //REQUERIDO TO ALIGN ICON APP TO RIGHT IN TOP
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(INVISIBLE);
        toolR.setVisibility(VISIBLE);


        spinner = (Spinner) view.findViewById(R.id.exp_list);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Selecciona una opción");
        categories.add("Masculino");
        categories.add("Femenino");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        toCuestionarioDos();

        return view;
    }



    public void toCuestionarioDos() {

        /*LinearLayout llTerm;
        llTerm = (LinearLayout) view.findViewById(R.id.llTerm);
        llTerm.setVisibility(INVISIBLE);*/

        btnCuestionarioUno = (Button) view.findViewById(R.id.btnCuestionarioUno);
        btnCuestionarioUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getRespuestas();
                if( valida_fecha() ) {
                    if (valida()) {
                        FCuestionarioDos fragmentCD = new FCuestionarioDos();
                        Bundle args = new Bundle();
                        args.putStringArray("respCuesUno", respCuesUno);
                        fragmentCD.setArguments(args);

                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.flInicio, fragmentCD, "fragmentCD");
                        ft.commit();
                    } else {
                        Toast.makeText(getContext(), "Campos por completar", Toast.LENGTH_SHORT).show();
                    }
                }  else {
                    Toast.makeText(getContext(), "Ingrese una fecha de nacimiento valida", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }





    public void getRespuestas(){

        EditText edtDia = (EditText) view.findViewById(R.id.edtDia);
        EditText edtMes = (EditText) view.findViewById(R.id.edtMes);
        EditText edtAnio = (EditText) view.findViewById(R.id.edtAnio);
        //Spinner  spnGenero = (Spinner) view.findViewById(R.id.exp_list);
        EditText edtEstatura = (EditText) view.findViewById(R.id.edtEstatura);
        EditText edtPesMax = (EditText) view.findViewById(R.id.edtPesMax);
        EditText edtPesMin = (EditText) view.findViewById(R.id.edtPsMin);
        EditText edtPesActual = (EditText) view.findViewById(R.id.edtPesAc);
        EditText edtPesDeseado = (EditText) view.findViewById(R.id.edtPesDes);
        EditText edtKgSobre = (EditText) view.findViewById(R.id.edtKgConsidSobrep);
        EditText edtPadecimi = (EditText) view.findViewById(R.id.edtPadec);
        EditText edtMedicament = (EditText) view.findViewById(R.id.edtMedicDiar);

        respCuesUno [0] = edtDia.getText().toString();
        respCuesUno [1] = edtMes.getText().toString();
        respCuesUno [2] = edtAnio.getText().toString();
        respCuesUno [3] = String.valueOf((spinner.getSelectedItemPosition()));
        respCuesUno [4] = edtEstatura.getText().toString();
        respCuesUno [5] = edtPesMax.getText().toString();
        respCuesUno [6] = edtPesMin.getText().toString();
        respCuesUno [7] = edtPesActual.getText().toString();
        respCuesUno [8] = edtPesDeseado.getText().toString();
        respCuesUno [9] = edtKgSobre.getText().toString();
        respCuesUno [10] = edtPadecimi.getText().toString();
        respCuesUno [11] = edtMedicament.getText().toString();
    }

    public boolean valida_fecha(){
        if( parseInt(respCuesUno[1]) > 12 || parseInt(respCuesUno[1]) < 1 )
            return false;

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int limit_age = year-Integer.parseInt(respCuesUno[2]);

        if( limit_age > 90 )
            return false;

        return true;
    }

    public boolean  valida(){
        for(int i=0; i<=11; i++){
           if( i!=3 && respCuesUno[i].equals("") )
             {return false;}
            else if ( i==3 && respCuesUno[i].equals("0"))
             {return false;}
        }
        return true;
    }
}
