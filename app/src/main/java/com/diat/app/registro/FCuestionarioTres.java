package com.diat.app.registro;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.diat.app.navigation.LandingActivityB;
import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FCuestionarioTres extends Fragment {
        private View view;
        private Spinner spVeint,spVeintUn,spVeinDos,spVeniTre,spVeincua,spVcinc, spVSei, spVNueve, spiTrein, spinTreUno;
        String [] respCuesTres = new String[40];
        private Button btnCuestionarioTres;
        private ArrayList<Spinner> spiners;
        private String idcat;
        private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.registro_frag_cuestionariotres, container, false);

        /*PARA HACER INVISIBLE EL FOOTER DE AVISOS DE PRIVACIDAD*/
        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        llTerm.setVisibility(View.GONE);

        //REQUERIDO TO ALIGN ICON APP TO RIGHT IN TOP
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(INVISIBLE);
        toolR.setVisibility(VISIBLE);

        respCuesTres =  getArguments().getStringArray("cuestionarioDos");
        //Toast.makeText(getContext(), "preubacTres: "+ respCuesTres[0]+respCuesTres[12], Toast.LENGTH_SHORT).show();


        spVeint = (Spinner)view.findViewById(R.id.spinn_veinte);
        spVeintUn = (Spinner)view.findViewById(R.id.spinn_vuno);
        spVeinDos = (Spinner)view.findViewById(R.id.spinn_vdos);
        spVeniTre = (Spinner)view.findViewById(R.id.spinn_vtres);
        spVeincua = (Spinner)view.findViewById(R.id.spinn_vcuatr);
        spVcinc = (Spinner)view.findViewById(R.id.spinn_vcin);
        spVSei = (Spinner)view.findViewById(R.id.spinn_vseis);
        spVNueve = (Spinner)view.findViewById(R.id.spinn_vnuev);
        spiTrein = (Spinner)view.findViewById(R.id.spinn_treint);
        spinTreUno = (Spinner)view.findViewById(R.id.spinn_treUno);



        //sppiner mujer
        List<String> spVeinte = new ArrayList<String>();
        spVeinte.add("Selecciona una opción");
        spVeinte.add("N/A");
        spVeinte.add("Sí");
        spVeinte.add("No");
        ArrayAdapter<String> dVeinte = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVeinte);
        dVeinte.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVeint.setAdapter(dVeinte);
        //spVeint.setEnabled(false);

        //sppiner mujer
        List<String> spVeintUno = new ArrayList<String>();
        spVeintUno.add("Selecciona una opción");
        spVeintUno.add("N/A");
        spVeintUno.add("Sí");
        spVeintUno.add("Soy irregular");
        spVeintUno.add("Soy irregular, en tratamiento");
        spVeintUno.add("No reglo");
        ArrayAdapter<String> dVeintuno = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVeintUno);
        dVeintuno.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVeintUn.setAdapter(dVeintuno);
        //spVeintUn.setEnabled(false);

        //sppiner mujer
        List<String> spVDos = new ArrayList<String>();
        spVDos.add("Selecciona una opción");
        spVDos.add("N/A");
        spVDos.add("De 7 a 12 kg");
        spVDos.add("De 13 a 16 kg");
        spVDos.add("Más de 17 kg");
        ArrayAdapter<String> dVeintdo = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVDos);
        dVeintdo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVeinDos.setAdapter(dVeintdo);
        //spVeinDos.setEnabled(false);

        //sppiner mujer
        List<String> spVtre = new ArrayList<String>();
        spVtre.add("Selecciona una opción");
        spVtre.add("N/A");
        spVtre.add("Sí");
        spVtre.add("No");
        spVtre.add("En alguno");
        ArrayAdapter<String> dVeinteTre = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVtre);
        dVeinteTre.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVeniTre.setAdapter(dVeinteTre);
        //spVeniTre.setEnabled(false);

        //sppiner mujer
        List<String> spVcuatro = new ArrayList<String>();
        spVcuatro.add("Selecciona una opción");
        spVcuatro.add("N/A");
        spVcuatro.add("Para reglar regular");
        spVcuatro.add("Para menopausia");
        spVcuatro.add("Otro");
        spVcuatro.add("No");
        ArrayAdapter<String> dVeintCuat = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVcuatro);
        dVeintCuat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVeincua.setAdapter(dVeintCuat);
        //spVeincua.setEnabled(false);

        List<String> spVCin = new ArrayList<String>();
        spVCin.add("Selecciona una opción");
        spVCin.add("Sí");
        spVCin.add("No");
        ArrayAdapter<String> dVcinc = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVCin);
        dVcinc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVcinc.setAdapter(dVcinc);


        List<String> spVSeis = new ArrayList<String>();
        spVSeis.add("Selecciona una opción");
        spVSeis.add("Sí");
        spVSeis.add("No");
        ArrayAdapter<String> dVseis = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVSeis);
        dVseis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVSei.setAdapter(dVseis);


        List<String> spVNuev = new ArrayList<String>();
        spVNuev.add("Selecciona una opción");
        spVNuev.add("Comer grandes cantidades en pocas comidas");
        spVNuev.add("Picar todo el día");
        spVNuev.add("Nunca se sabe");
        ArrayAdapter<String> dVnuev = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spVNuev);
        dVnuev.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spVNueve.setAdapter(dVnuev);



        List<String> spTrint = new ArrayList<String>();
        spTrint.add("Selecciona una opción");
        spTrint.add("Sí");
        spTrint.add("No");
        spTrint.add("Por días o temporadas");
        ArrayAdapter<String> dTreinta = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spTrint);
        dTreinta.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiTrein.setAdapter(dTreinta);


        List<String> spTrUno = new ArrayList<String>();
        spTrUno.add("Selecciona una opción");
        spTrUno.add("Sí");
        spTrUno.add("No");
        spTrUno.add("Solo por mi sobrepeso");
        ArrayAdapter<String> dTreintaUno = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spTrUno);
        dTreintaUno.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinTreUno.setAdapter(dTreintaUno);

        if(respCuesTres[3] == "1") { //if is man
            spVeint.setSelection(1);//Set N/a
            spVeint.setEnabled(false);

            spVeintUn.setSelection(1);//Set N/a
            spVeintUn.setEnabled(false);

            spVeinDos.setSelection(1);//Set N/a
            spVeinDos.setEnabled(false);

            spVeniTre.setSelection(1);//Set N/a
            spVeniTre.setEnabled(false);

            spVeincua.setSelection(1);//Set N/a
            spVeincua.setEnabled(false);

        }



        toRegistroPago();

        return view;
    }


    public void toRegistroPago() {

        /*LinearLayout llTerm;
        llTerm = (LinearLayout) view.findViewById(R.id.llTerm);
        llTerm.setVisibility(INVISIBLE);*/

        btnCuestionarioTres = (Button) view.findViewById(R.id.btnCuestTres);
        btnCuestionarioTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    getRespuestasCuestionarioTres();

                if(valida()) {
                        saveCuestionario();


                    }
                    else{  Toast.makeText(getContext(), "Campos por completar", Toast.LENGTH_SHORT).show();}
            }
        });


    }


    /**METODO QUE GUARDA EL CUESTIONARIO UNA VEZ VALIDADOS LOS CAMPOS*/
    public void saveCuestionario(){
        //NECESARIO PARA RECUPERAR idcat GENERADO DURANTE EL REGISTRO
        SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        idcat = userDet.getString("idcat", "");



                    new Servicios(getContext()).cuestionario(Arrays.asList(
                                    respCuesTres[2] + "-" +respCuesTres[1]+ "-"+ respCuesTres[0],
                                    respCuesTres[3],
                                    respCuesTres[4],
                                    respCuesTres[5],
                                    respCuesTres[6],
                                    respCuesTres[7],
                                    respCuesTres[8],
                                    respCuesTres[9],
                                    respCuesTres[10],
                                    respCuesTres[11],
                                    respCuesTres[12],
                                    respCuesTres[13],
                                    respCuesTres[14],
                                    respCuesTres[15],
                                    respCuesTres[16],
                                    respCuesTres[17],
                                    respCuesTres[18],
                                    respCuesTres[19],
                                    respCuesTres[20],
                                    respCuesTres[21],
                                    respCuesTres[22],
                                    respCuesTres[23],
                                    respCuesTres[24],
                                    respCuesTres[25],
                                    respCuesTres[26],
                                    respCuesTres[27],
                                    respCuesTres[28],
                                    respCuesTres[29],
                                    respCuesTres[30],
                                    respCuesTres[31],
                                    respCuesTres[32],
                                    respCuesTres[33],
                                    respCuesTres[34],
                                    respCuesTres[35],
                                    idcat
                            ),

                            new Servicios.DataCallback() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result){
                                        sharedPreferences = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                        /**if es un user add by admin redirect to welcome ...*/
                                        if(!sharedPreferences.getString("idCompania", "").equals("null") && sharedPreferences.getString("cuestionario", "").equals("null"))
                                        {
                                            getActivity().finish();
                                            startActivity(new Intent(getContext(), LandingActivityB.class));
                                            SharedPreferences userDetails = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                                            userDetails.edit().putString("cuestionario", "1").apply();
                                            userDetails.edit().commit();
                                        }
                                        else {
                                            final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                            ft.replace(R.id.flInicio, new FPago(), "NewFragmentPay");
                                            ft.commit();
                                        }

                                   }

                                }
                            });
                }






    private void getRespuestasCuestionarioTres(){

        EditText edtcualalimnstDis = (EditText) view.findViewById(R.id.edtVCin);
        EditText edtalimentossanos = (EditText) view.findViewById(R.id.edtVeintisiet);
        EditText edtalimentosChata = (EditText) view.findViewById(R.id.edtPreVeinOch);



        respCuesTres [23] = String.valueOf(spVeint.getSelectedItemPosition());
        respCuesTres [24] = String.valueOf(spVeintUn.getSelectedItemPosition());
        respCuesTres [25] = String.valueOf(spVeinDos.getSelectedItemPosition());
        respCuesTres [26] = edtcualalimnstDis.getText().toString();
        respCuesTres [27] = String.valueOf(spVeniTre.getSelectedItemPosition());
        respCuesTres [28] = String.valueOf(spVeincua.getSelectedItemPosition());
        respCuesTres [29] = String.valueOf(spVcinc.getSelectedItemPosition());
        respCuesTres [30] = String.valueOf(spVSei.getSelectedItemPosition());
        respCuesTres [31] = edtalimentossanos.getText().toString();
        respCuesTres [32] = edtalimentosChata.getText().toString();
        respCuesTres [33] = String.valueOf(spVNueve.getSelectedItemPosition());
        respCuesTres [34] = String.valueOf(spiTrein.getSelectedItemPosition());
        respCuesTres [35] = String.valueOf(spinTreUno.getSelectedItemPosition());




    }




    public boolean  valida(){

        for(int i=23; i<=35; i++){
            if( i!=26 && i!=31 && i!=32 && respCuesTres[i].equals("0") )
            {return false;}

            else if(i == 26 && respCuesTres [29].equals("1")){
                if(respCuesTres [26].equals("")){
                    return false;
                }
            }
            else if ((i==31 ||i==32 ) && (respCuesTres[i].equals("")))
            {
                return false;
            }
        }
        return true;
    }



}
