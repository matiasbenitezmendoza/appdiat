package com.diat.app.registro;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.diat.app.diat.FragmentInicio;
import com.diat.app.diat.R;

import java.util.Arrays;

import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;


public class FTipoPlan extends Fragment {

    private View view;
    private ListVATipoPlan adapter;
    private String tipo_Plan;
    String[] titulo;
    private Button btnFooterTipoPlan;
    private String positionSelecLisTipoRegimen = "-1";
    private String[] datUser = new String[4];
    private String regi;
    private String tipPlan;
    private String idcat;
    private FragmentManager manager;
    private SharedPreferences userDet;
    private String isLoged;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.registro_frag_tipoplan, container, false);


        /*PARA HACER INVISIBLE EL FOOTER DE AVISOS DE PRIVACIDAD*/
        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        if(llTerm !=null) {
            llTerm.setVisibility(View.GONE);
        }


        userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        isLoged = userDet.getString("isLoged", "");

        if (!isLoged.equals("1")) {
            datUser = getArguments().getStringArray("datosUsuario");
            createListPlan();
            tocuestionarioUno();
        } else {
            createListPlan();
            toRenovarPlan();
        }


        return view;
    }


    public void createListPlan() {

        titulo = new String[]{"Experto", "Comprometido", "Estilo de vida", "Audaz", "VIP"};

        String[] textos = new String[]{
                "\nInscríbete por un mes: $699 mensual (cargo recurrente a la tarjeta de crédito).\n",
                "\nInscríbete por seis meses: $2,999 en un solo pago. Ahórra más de $1000\n",
                "\nInscríbete por un año: $4,999 en un solo pago. Ahórra más de $3000\n",
                "\nUn mes de consultas semanales, vía telefónica con horario predeterminado (15 min de duración). Requiere registrar su peso previo a la cita. Cambios y cancelaciones solo 48 horas antes de la consulta. $1,199 pago mensual efectuado previo a las consultas.\n",
                "\nVIP vía Skype $1,999. Requiere registrar su peso previo a la cita (duración 30 min). Cambios y cancelaciones solo 48 horas antes de la consulta. Las citas serán cargadas al cliente al momento de acordar horario.\n"
        };


        final ListView lista = (ListView) view.findViewById(R.id.lvTipoPlan);
        adapter = new ListVATipoPlan(this.getContext(), titulo, textos);

        //To add footer to list view
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.registro_lay_foot_tipoplan, lista, false);
        lista.addFooterView(footer, null, false);

        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                view.setSelected(true);
                positionSelecLisTipoRegimen = String.valueOf(position);

                switch (position) {
                    case 0:
                        tipo_Plan = titulo[position];
                        break;
                    case 1:
                        tipo_Plan = titulo[position];
                        break;
                    case 2:
                        tipo_Plan = titulo[position];
                        break;
                    case 3:
                        tipo_Plan = titulo[position];
                        break;
                    case 4:
                        tipo_Plan = titulo[position];
                        break;
                    default:
                        break;
                }

                SharedPreferences userDetails = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                SharedPreferences.Editor edit = userDetails.edit();

                //edit.putString("tipoPlan",  tp.trim());
                userDetails.edit().putString("tipoPlan", tipo_Plan.trim()).apply();
                edit.commit();


                SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                idcat = userDet.getString("idcat", "");
                regi = userDet.getString("regimen", "");
                tipPlan = userDet.getString("tipoPlan", "");
            }


        });

    }


    /**
     * REDIRIGE A CUESTIONARIO UNO AL COMPROBAR QUE SE HA SELECCIONADO UN TIPO DE PLAN
     **/
    public void tocuestionarioUno() {


        btnFooterTipoPlan = (Button) view.findViewById(R.id.btnFooterTipoPlan);
        btnFooterTipoPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Integer.valueOf(positionSelecLisTipoRegimen) != -1) {
                    new Servicios(getContext()).registro(Arrays.asList(
                            datUser[0],
                            datUser[1],
                            datUser[2],
                            datUser[3],
                            regi,
                            tipPlan,
                            "1"
                            ),

                            new Servicios.DataCallback() {
                                @Override
                                public void onResult(boolean result) {
                                    if (result) {
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.replace(R.id.flInicio, new FCuestionarioUno(), "toFCuestionarioUno");
                                        ft.commit();
                                    } else {//REDIRECT TO START
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.replace(R.id.flInicio, new FragmentInicio(), "FInicio");
                                        //ft.addToBackStack(null);IF EL USER IS REGISTERED NO REGRESE A LA PANTALLA DE TIPO PLAN
                                        ft.commit();

                                        //MOSTRAR LA HAMBURGUER
                                        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
                                        toolC.setVisibility(View.VISIBLE);
                                    }


                                }
                            });
                } else {
                    Toast.makeText(getContext(), "Seleccione un tipo de plan", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * metodo para renovacion de plan
     */
    private void toRenovarPlan() {

        btnFooterTipoPlan = (Button) view.findViewById(R.id.btnFooterTipoPlan);
        btnFooterTipoPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Integer.valueOf(positionSelecLisTipoRegimen) != -1) {
                    final FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.flInicio, new FPago(), "NewFragmentPay");
                    ft.commit();
                } else

                {
                    Toast.makeText(getContext(), "Seleccione un tipo de plan", Toast.LENGTH_SHORT).show();
                }
            }


        });
    }
}
