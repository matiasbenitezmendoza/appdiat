package com.diat.app.registro;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.diat.app.auxclases.Constantes;
import com.diat.app.diat.FragmentInicio;
import com.diat.app.diat.R;

import java.util.Arrays;
import java.util.HashMap;

import com.diat.app.navigation.LandingActivityB;
import com.diat.app.webservices.Servicios;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.diat.app.auxclases.Constantes.PayKeys.idusuario;

public class FPago extends Fragment {

    private EditText edtNombTarjet;
    private EditText edtNumTarjet;
    private EditText edtMesTarjet;
    private EditText edtAnioTarjet;
    private EditText edtCvc;
    private EditText edtCodDesc;
    private Button btnPago;
    private SharedPreferences sharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.registro_frag_pago, container, false);
        edtNombTarjet = (EditText) view.findViewById(R.id.edtNombTarjet);
        edtNumTarjet = (EditText) view.findViewById(R.id.edtNumTarje);
        edtMesTarjet = (EditText) view.findViewById(R.id.edtMesTarjet);
        edtAnioTarjet = (EditText) view.findViewById(R.id.edtAnioTarjet);
        edtCvc = (EditText) view.findViewById(R.id.edtCVC);
        edtCodDesc = (EditText) view.findViewById(R.id.edtCodDesc);
        btnPago = (Button) view.findViewById(R.id.btnPago);
        sharedPreferences = getActivity().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        showToolCenter();
        setAction();
        return view;
    }

<<<<<<< HEAD

    public void showToolCenter() {
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(VISIBLE);
        toolR.setVisibility(INVISIBLE);
    }


    /**REDIRIGE A LOGIN EN CASO DE QUE LA OPERACION SEA EXITOSA**/
    public void toRegistro() {

        /*LinearLayout llTerm;
        llTerm = (LinearLayout) view.findViewById(R.id.llTerm);
        llTerm.setVisibility(INVISIBLE);*/

        final EditText nomTarjeta =  (EditText) view.findViewById(R.id.edtNombTarjet);
        final EditText numtarjeta =  (EditText) view.findViewById(R.id.edtNumTarje);
        final EditText mesTarje = (EditText) view.findViewById(R.id.edtMesTarjet);
        final EditText anioTarje = (EditText) view.findViewById(R.id.edtanio);
        final EditText cvc =  (EditText) view.findViewById(R.id.edtCVC);

        codDesc =  (EditText) view.findViewById(R.id.edtCodDesc);
        butnPago = (Button) view.findViewById(R.id.btnPago);
        if(isLogedUser.equals("1")) {
            butnPago.setText("FINALIZAR PAGO");
        } else {
            butnPago.setText("FINALIZAR REGISTRO");
        }

        butnPago.setOnClickListener(new View.OnClickListener() {
=======
    private void setAction() {
        btnPago.setOnClickListener(new View.OnClickListener() {
>>>>>>> origin/master
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtNombTarjet.getText()) ||
                        !TextUtils.isEmpty(edtNumTarjet.getText()) ||
                        !TextUtils.isEmpty(edtMesTarjet.getText()) ||
                        !TextUtils.isEmpty(edtAnioTarjet.getText()) ||
                        !TextUtils.isEmpty(edtCvc.getText())) {
                    new Servicios(getContext()).payCard(
                            getActivity(),
                            Arrays.asList(edtNombTarjet.getText().toString(),
                                    edtNumTarjet.getText().toString(),
                                    edtCvc.getText().toString(),
                                    edtMesTarjet.getText().toString(),
                                    edtAnioTarjet.getText().toString()),
                            mapa()
                            , new Servicios.DataCallback() {
                                @Override
                                public void onResult(boolean result) {
                                    showReference();
                                    if (sharedPreferences.getString("isLoged", "").equals("1")) {
                                        startActivity(new Intent(getContext(), LandingActivityB.class));
                                        getActivity().finish();
                                    } else {
                                        final FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.replace(R.id.flInicio, new FragmentInicio(), "FInicio");
                                        ft.commit();
                                    }
                                }
                            });
                }
            }
        });
    }

    private HashMap<String, String> mapa() {
        HashMap<String, String> params = new HashMap<>();
        params.put(idusuario, sharedPreferences.getString("idcat", ""));
        switch (sharedPreferences.getString("tipoPlan", "")) {
            case "Experto":
                params.put(Constantes.PayKeys.monto, "699");
                break;
            case "Comprometido":
                params.put(Constantes.PayKeys.monto, "2999");
                break;
            case "Estilo de vida":
                params.put(Constantes.PayKeys.monto, "4999");
                break;
            case "Audaz":
                params.put(Constantes.PayKeys.monto, "1199");
                break;
            case "VIP":
                params.put(Constantes.PayKeys.monto, "1999");
                break;
            default:
                break;
        }
        params.put(Constantes.PayKeys.mensaje, sharedPreferences.getString("tipoPlan", ""));
        params.put(Constantes.PayKeys.descuento, edtCodDesc.getText().toString());
        return params;
    }

    public void showToolCenter() {
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        ImageView toolH = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolH.setVisibility(INVISIBLE);
        toolC.setVisibility(VISIBLE);
        toolR.setVisibility(INVISIBLE);
    }

    private void showReference() {
        SharedPreferences varReferenceCargo = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
        String referenciaCargo = varReferenceCargo.getString("referenciaCargo", "");
        String flagFueRegistro = varReferenceCargo.getString("flagFueRegistro", "");
        if (!referenciaCargo.equals("") && flagFueRegistro.equals("1")) {
            Toast.makeText(getContext(), getString(R.string.referencia_cargo) + referenciaCargo, Toast.LENGTH_SHORT).show();
        }
    }
}
