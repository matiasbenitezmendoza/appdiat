package com.diat.app.registro;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.diat.R;

public class ListViewAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    String[] titulos;
    String[] textos;
    int[] imagenes;
    LayoutInflater inflater;

    public ListViewAdapter(Context context, String[] titulos, int[] imagenes, String[] textos) {
        this.context = context;
        this.titulos = titulos;
        this.imagenes = imagenes;
        this.textos = textos;
    }

    @Override
    public int getCount() {
        return titulos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        TextView txtTexto;  TextView tvTitulo;
        ImageView imgImg;

        //http://developer.android.com/intl/es/reference/android/view/LayoutInflater.html
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.registro_list_row_regimen, parent, false);

        // Locate the TextViews in listview_item.xml
        txtTexto = (TextView) itemView.findViewById(R.id.list_row_title);
        imgImg = (ImageView) itemView.findViewById(R.id.list_row_image);
        tvTitulo = (TextView) itemView.findViewById(R.id.tvTitulo);

        // Capture position and set to the TextViews
        txtTexto.setText(textos[position]);
        imgImg.setImageResource(imagenes[position]);
        tvTitulo.setText(titulos[position]);

        return itemView;
    }
}
