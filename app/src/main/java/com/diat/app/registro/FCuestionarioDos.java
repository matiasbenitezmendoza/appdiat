package com.diat.app.registro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.diat.app.diat.R;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static java.lang.String.valueOf;


public class FCuestionarioDos extends Fragment {
private View view;
    private Spinner spinner, spinnerDoce, spinnerTrec, spinnerCatoc, spinnerQuin, spinnerDieysei, spinnerDieysie, spinnerDiesyOch;
    private String [] respCuesDos = new String[40];
    private Button btnCuestionarioDos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.registro_frag_cuestionariodos, container, false);


          /*PARA HACER INVISIBLE EL FOOTER DE AVISOS DE PRIVACIDAD*/
        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        llTerm.setVisibility(View.GONE);

        //REQUERIDO TO ALIGN ICON APP TO RIGHT IN TOP
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.imgToolbarCenter);
        ImageView toolR = (ImageView) getActivity().findViewById(R.id.imgToolbarRigt);
        toolC.setVisibility(INVISIBLE);
        toolR.setVisibility(VISIBLE);

        respCuesDos =  getArguments().getStringArray("respCuesUno");
        //Toast.makeText(getContext(), "preuba: "+ respCuesDos[0], Toast.LENGTH_SHORT).show();




        spinner = (Spinner)view.findViewById(R.id.spinn_extrenimi);
        spinnerDoce = (Spinner)view.findViewById(R.id.spinn_doce);
        spinnerTrec = (Spinner)view.findViewById(R.id.spinn_trece);
        spinnerCatoc = (Spinner)view.findViewById(R.id.spinn_cator);
        spinnerQuin = (Spinner)view.findViewById(R.id.spinn_quinc);
        spinnerDieysei= (Spinner)view.findViewById(R.id.spinn_diesisei);
        spinnerDieysie = (Spinner)view.findViewById(R.id.spinn_diezysie);
        spinnerDiesyOch = (Spinner)view.findViewById(R.id.spinn_diesyoch);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Selecciona una opción");
        categories.add("No");
        categories.add("Moderado");
        categories.add("Sí");
        categories.add("Muy pronunciado/crónico");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);




        List<String> actFis = new ArrayList<String>();
        actFis.add("Selecciona una opción");
        actFis.add("3 hr por semana");
        actFis.add("4-7 hr por semana");
        actFis.add("más de 7 hr");
        actFis.add("Ninguno");
        ArrayAdapter<String> dAactFisic = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, actFis);
        dAactFisic.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDoce.setAdapter(dAactFisic);




        List<String> spTrec = new ArrayList<String>();
        spTrec.add("Selecciona una opción");
        spTrec.add("2 hr por semana");
        spTrec.add("3-5 hr por semana");
        spTrec.add("más de 5 hr por semana");
        spTrec.add("Ninguno");
        ArrayAdapter<String> dATrece = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spTrec);
        dATrece.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTrec.setAdapter(dATrece);


        List<String> spCato = new ArrayList<String>();
        spCato.add("Selecciona una opción");
        spCato.add("Sí");
        spCato.add("No");
        spCato.add("Ocasional");
        ArrayAdapter<String> dACato = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spCato);
        dACato.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCatoc.setAdapter(dACato);


        List<String> spQuin = new ArrayList<String>();
        spQuin.add("Selecciona una opción");
        spQuin.add("No");
        spQuin.add("Menos de 4 por semana");
        spQuin.add("4 o más por semana");
        ArrayAdapter<String> dAquin = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spQuin);
        dAquin.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerQuin.setAdapter(dAquin);


        List<String> spDSeis = new ArrayList<String>();
        spDSeis.add("Selecciona una opción");
        spDSeis.add("Menos de 5 vasos");
        spDSeis.add("Más de 5 vasos");
        ArrayAdapter<String> dDSis = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spDSeis);
        dDSis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDieysei.setAdapter(dDSis);



        List<String> spDSie = new ArrayList<String>();
        spDSie.add("Selecciona una opción");
        spDSie.add("Menos de 5 veces por día");
        spDSie.add("Más de 5 veces por día");
        ArrayAdapter<String> dDSiet = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spDSie);
        dDSiet.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDieysie.setAdapter(dDSiet);

        List<String> spDOch = new ArrayList<String>();
        spDOch.add("Selecciona una opción");
        spDOch.add("No");
        spDOch.add("Naturistas");
        spDOch.add("Medicamentos controlados");
        ArrayAdapter<String> dDOch = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_item, spDOch);
        dDOch.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDiesyOch.setAdapter(dDOch);

        toCuestionarioTres();

        return  view;
    }



    public void toCuestionarioTres() {

        /*LinearLayout llTerm;
        llTerm = (LinearLayout) view.findViewById(R.id.llTerm);
        llTerm.setVisibility(INVISIBLE);*/

        btnCuestionarioDos = (Button) view.findViewById(R.id.btnCuestionarioDos);
        btnCuestionarioDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getRespuestasCuestionarioDos();

                if(valida()){
                    FCuestionarioTres fragmentCD = new FCuestionarioTres();
                    Bundle args = new Bundle();
                    args.putStringArray("cuestionarioDos", respCuesDos);
                    fragmentCD.setArguments(args);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.flInicio, fragmentCD, "fragmentCD");
                    ft.commit();
                   }
                else{  Toast.makeText(getContext(), "Campos por completar", Toast.LENGTH_SHORT).show();}
            }
        });


    }




   private void getRespuestasCuestionarioDos(){


       EditText edtCualesBebidas = (EditText) view.findViewById(R.id.edtQuin);
       EditText edtCualesMedica = (EditText) view.findViewById(R.id.edtDiesioc);
       EditText edtenferFamilares = (EditText) view.findViewById(R.id.edtEnferFamiliares);

       respCuesDos [12] = String.valueOf((spinner.getSelectedItemPosition()));
       respCuesDos [13] = String.valueOf((spinnerDoce.getSelectedItemPosition()));
       respCuesDos [14] = String.valueOf((spinnerTrec.getSelectedItemPosition()));
       respCuesDos [15] = String.valueOf((spinnerCatoc.getSelectedItemPosition()));
       respCuesDos [16] = String.valueOf((spinnerQuin.getSelectedItemPosition()));
       respCuesDos [17] = edtCualesBebidas.getText().toString();
       respCuesDos [18] = String.valueOf((spinnerDieysei.getSelectedItemPosition()));
       respCuesDos [19] = String.valueOf((spinnerDieysie.getSelectedItemPosition()));
       respCuesDos [20] = String.valueOf((spinnerDiesyOch.getSelectedItemPosition()));
       respCuesDos [21] = edtenferFamilares.getText().toString();
       respCuesDos [22] = edtCualesMedica.getText().toString();

    }

    public boolean  valida(){
        for(int i=12; i<=22; i++){
            if( i!=17 && i!=21 && i !=22 && respCuesDos[i].equals("0") )
            {return false;}

            else if (i == 17 && !respCuesDos [16].equals("1")){
                if(respCuesDos [17].equals("")){
                    return false;
                }
            }
            else if (i == 22 && !respCuesDos [20].equals("1")){
                    if(respCuesDos [22].equals("")){
                        return false;
                    }
            }

            else if ((i ==21) && respCuesDos[i].equals(""))
            {
                return false;
            }

        }
        return true;
    }




}
