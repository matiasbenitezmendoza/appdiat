package com.diat.app.registro;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.diat.app.diat.R;

import static android.content.Context.MODE_PRIVATE;


public class FRegistroRegimen extends Fragment {

     private View view;
     ListViewAdapter adapter;
     private Button btnFootRegistro;
     private String positionSelecLisTipoRegimen="-1";
     private String [] datUser = new String[4];

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.registro_frag_regimen, container, false);

        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        llTerm.setVisibility(View.GONE);


        datUser =  getArguments().getStringArray("datosUsuario");

        creaLista();
        totipoPlan();

        return view;
    }





    public void creaLista(){

        String[] titulo = new String[]{
                "Práctico", "Ejecutivo",  "Militar", "Místico"};

        String[] textos = new String[]{
                "\nApego a la propuesta lo más preciso que sus tareas se lo permitan.\n",
                "\nEste plan está diseñado con base a las necesidades prácticas de tu rutina.\nComidas en restaurantes y cafeterías.\nLa dieta diaria requiere de poca planeación y organización de menús.\nIdeal para personas que trabajan o están constantemente fuera de casa.\n",
                "\nEste es un plan ideado para personas que quieren alcanzar su objetivo rápidamente y están dispuestas a hacer los sacrificios necesarios.\nAnteponen la dieta a los eventos sociales o profesionales.\n",
                "\nEstá diseñado para personas que aspiran a más que un cambio en su físico: una transformación en su ser.\nIntegrarán la propuesta de alimentación a su estilo de vida permanentemente.\n"
        };



        int[] imagenes = {
                R.drawable.ico_practico, R.drawable.ico_ejecutivo, R.drawable.ico_militar, R.drawable.ico_mistico
        };


        ListView lista = (ListView) view.findViewById(R.id.listView1);
        lista.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        adapter = new ListViewAdapter(this.getContext(), titulo, imagenes, textos);
        //To add footer to list view
        LayoutInflater inflater = getActivity().getLayoutInflater();
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.registro_lay_foot_regimen, lista, false);
        lista.addFooterView(footer, null, false);
        lista.setAdapter(adapter);




        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                view.setSelected(true);

                positionSelecLisTipoRegimen = String.valueOf(position);

                SharedPreferences userDetails = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                SharedPreferences.Editor edit = userDetails.edit();
                //edit.clear();
                userDetails.edit().putString("regimen",  positionSelecLisTipoRegimen.trim()).apply();
                edit.commit();


                SharedPreferences userDet = getContext().getSharedPreferences("PreferencesRegisto", MODE_PRIVATE);
                String regi = userDet.getString("regimen", "");
                String idcat = userDet.getString("idcat", "");
                //Toast.makeText(getContext(), "idcat: " +idcat+ " rgi "+regi, Toast.LENGTH_SHORT).show();


                }

          });

        }





/**REDIRIGE A TIPO DE PLAN  AL COMPROBAR QUE SE HA SELECCIONADO UN TIPO DE REGIMEN**/
    public void totipoPlan() {

        /*LinearLayout llTerm;
        llTerm = (LinearLayout) view.findViewById(R.id.llTerm);
        llTerm.setVisibility(INVISIBLE);*/

        btnFootRegistro = (Button) view.findViewById(R.id.btnFootRegistro);
        btnFootRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(Integer.valueOf(positionSelecLisTipoRegimen) != -1) {


                    FTipoPlan fragTipoPlan = new FTipoPlan();
                    Bundle args = new Bundle();
                    args.putStringArray("datosUsuario", datUser);
                    fragTipoPlan.setArguments(args);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.flInicio, fragTipoPlan, "fTipPlan");
                    //ft.addToBackStack(null);
                    ft.commit();
                }
                else{  Toast.makeText(getContext(), "Seleccione un tipo de regimen", Toast.LENGTH_SHORT).show();}
            }
        });


      }


    }



