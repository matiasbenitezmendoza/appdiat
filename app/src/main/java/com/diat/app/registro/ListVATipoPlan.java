package com.diat.app.registro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.diat.app.diat.R;


public class ListVATipoPlan extends BaseAdapter{
    Context context;
    String[] titulos;
    String[] textos;
    LayoutInflater inflater;

        public ListVATipoPlan(Context context, String[] titulos, String[] textos) {
            this.context = context;
            this.titulos = titulos;
            this.textos = textos;
        }

        @Override
        public int getCount() {
            return titulos.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            TextView txtTexto;  TextView tvTitulo;

            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View itemView = inflater.inflate(R.layout.registro_list_row_tipoplan, parent, false);


            txtTexto = (TextView) itemView.findViewById(R.id.list_row_titleTipoPlan);
            tvTitulo = (TextView) itemView.findViewById(R.id.tvTituloTipoPlan);

            txtTexto.setText(textos[position]);
            tvTitulo.setText(titulos[position]);

            return itemView;
        }

}
