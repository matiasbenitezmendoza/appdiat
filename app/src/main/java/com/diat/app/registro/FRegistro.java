package com.diat.app.registro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.diat.app.auxclases.Validaciones;
import com.diat.app.diat.MainActivity;
import com.diat.app.diat.R;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.Arrays;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;



public class FRegistro extends Fragment {

    private View view;
    private Switch mySwitch;
    private Button botonFace;
    private Button btnRegistro;
    private Button btnRegistFace;
    private ArrayList<EditText> textos;
    private String re;
    private String [] datosUser = new String[4];



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.registro_frag_registro, container, false);
        //SE OCULTA EN ESTA PANTALLA LA HAMBURGER
        ImageView toolC = (ImageView) getActivity().findViewById(R.id.hamburgerMenu);
        toolC.setVisibility(INVISIBLE);


        /*-----------------CAMBIO DE ESTADO DEL SWITCH-------------------------------------------*/
        mySwitch = (Switch) view.findViewById(R.id.swRegist);
        botonFace = (Button) view.findViewById(R.id.btnRegistroFacebook);
        btnRegistro = (Button) view.findViewById(R.id.btnRegistro);
        botonFace.setAlpha(0.6f);
        btnRegistro.setAlpha(0.6f);

        //mySwitch.setChecked(true);

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    botonFace.setEnabled(true);
                    btnRegistro.setEnabled(true);
                    botonFace.setAlpha(1f);
                    btnRegistro.setAlpha(1f);
                }
                else {
                    botonFace.setEnabled(false);
                    btnRegistro.setEnabled(false);
                    botonFace.setAlpha(0.6f);
                    btnRegistro.setAlpha(0.6f);
                }
            }
        });
        /*---------------------------------------------------------------------------------------*/

        //uncoment when use the petition to server
        registro();


        LinearLayout llTerm;
        llTerm = (LinearLayout) getActivity().findViewById(R.id.llTerm);
        llTerm.setVisibility(VISIBLE);
        return view;
    }




    public void registro(){

        textos = new ArrayList<>();
        textos.add((EditText) view.findViewById(R.id.edtNombre));
        textos.add((EditText) view.findViewById(R.id.edtapellido));
        textos.add((EditText) view.findViewById(R.id.edtCorreo));
        textos.add((EditText) view.findViewById(R.id.edtContrasenia));
        textos.add((EditText) view.findViewById(R.id.edtContraseniaConfirmar));
        btnRegistro = (Button) view.findViewById(R.id.btnRegistro);
        btnRegistFace = (Button) view.findViewById(R.id.btnRegistroFacebook);


        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Validaciones.camposCompletos(textos))
                    Toast.makeText(getContext(), getString(R.string.campo_vacio), Toast.LENGTH_SHORT).show();
                else if (!Validaciones.isEmail(textos.get(textos.indexOf(view.findViewById(R.id.edtCorreo))).getText().toString()))
                    Toast.makeText(getContext(), getString(R.string.formato_incorrecto), Toast.LENGTH_SHORT).show();
                else if (!textos.get(textos.indexOf(view.findViewById(R.id.edtContrasenia))).getText().toString().equals(textos.get(textos.indexOf(view.findViewById(R.id.edtContraseniaConfirmar))).getText().toString()))
                    Toast.makeText(getContext(), getString(R.string.contrasenia_no_coincide), Toast.LENGTH_SHORT).show();
                else {

                    datosUser[0] = textos.get(textos.indexOf(view.findViewById(R.id.edtNombre))).getText().toString();
                    datosUser[1] = textos.get(textos.indexOf(view.findViewById(R.id.edtapellido))).getText().toString();
                    datosUser[2] = textos.get(textos.indexOf(view.findViewById(R.id.edtCorreo))).getText().toString();
                    datosUser[3] = textos.get(textos.indexOf(view.findViewById(R.id.edtContrasenia))).getText().toString();


                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                        fm.popBackStack();
                    }

                    FRegistroRegimen fragRegReg = new FRegistroRegimen();
                    Bundle args = new Bundle();
                    args.putStringArray("datosUsuario", datosUser);
                    fragRegReg.setArguments(args);


                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.flInicio, fragRegReg, "fRegRegimen");
                    //ft.addToBackStack(null);
                    ft.commit();

                    }
            }
        });

        btnRegistFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fbCase = 2;
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));

                FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
            }
        });




    }









}
