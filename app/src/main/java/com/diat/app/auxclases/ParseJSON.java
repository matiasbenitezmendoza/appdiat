package com.diat.app.auxclases;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseJSON {
    public static String[] idtip;
    public static String[] tip;


    public static final String JSON_ARRAY = "result";
    public static final String KEY_ID = "idtip";
    public static final String KEY_NAME = "tip";


    private JSONArray users = null;

    private String json;

    public ParseJSON(String json){
        this.json = json;
    }

    public void parseJSON(){
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            users = jsonObject.getJSONArray(JSON_ARRAY);

            idtip = new String[users.length()];
            tip = new String[users.length()];


            for(int i=0;i<users.length();i++){
                JSONObject jo = users.getJSONObject(i);
                idtip[i] = jo.getString(KEY_ID);
                tip[i] = jo.getString(KEY_NAME);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}