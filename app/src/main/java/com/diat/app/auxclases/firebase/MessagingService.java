package com.diat.app.auxclases.firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.diat.app.navigation.LandingActivityB;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MessagingService extends FirebaseMessagingService {

    private static final String TAG = MessagingService.class.getSimpleName();
    private NotificationUtils NotificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("Token", "From: " + remoteMessage.getFrom());
        if (remoteMessage == null)
            return;

        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            JsonObject json = (new JsonParser().parse(remoteMessage.getData().toString())).getAsJsonObject();
            handleDataMessage(json);
        }
    }

    private void handleNotification(String message) {
        if (!com.diat.app.auxclases.firebase.NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(FirebaseKeys.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            //notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JsonObject json) {
        Log.e(TAG, "push json: " + json.toString());
        SharedPreferences sharedPreferences = getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE);

        JsonObject data = json.getAsJsonObject("data");
        int tipo = data.get("tipo").getAsInt();
        String title;
        String message = data.get("mensaje").getAsString();
        String timestamp = data.get("timestamp").getAsString();
        Intent resultIntent = new Intent(getApplicationContext(), LandingActivityB.class);
        resultIntent.putExtras(new Bundle());
        resultIntent.putExtra("PRUEBA", "PRUEBA");
        sharedPreferences.edit().putInt("tipo", tipo).commit();

        if (!com.diat.app.auxclases.firebase.NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Log.e("Background",""+tipo);
            switch (tipo) {
                case 1:
                    title = "Tip Recibido";
                    break;
                case 2:
                    title = "Dieta Lista";
                    break;
                case 3:
                    title = "Pago requerido";
                    break;
                case 4:
                    title = "Nuevo Mensaje";
                    break;
                case 5:
                    title = "Cita Aceptada";
                    break;
                default:
                    title = "DIÄT";
                    break;
            }
            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, tipo);
            Intent pushNotification = new Intent(FirebaseKeys.PUSH_NOTIFICATION);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        } else {
            // app is in background, show the notification in notification tray
            Log.e("Background 2",""+tipo);
            resultIntent.putExtra("notificaciones", tipo);
            switch (tipo) {
                case 1:
                    title = "Tip Recibido";
                    break;
                case 2:
                    title = "Dieta Lista";
                    break;
                case 3:
                    title = "Pago requerido";
                    break;
                case 4:
                    title = "Nuevo Mensaje";
                    break;
                case 5:
                    title = "Cita Aceptada";
                    break;
                default:
                    title = "DIÄT";
                    break;
            }

            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, tipo);
        }

    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent, int tipo) {
        NotificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        NotificationUtils.showNotificationMessage(title, message, timeStamp, intent, tipo);
    }
}
