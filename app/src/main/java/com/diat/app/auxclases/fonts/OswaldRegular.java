package com.diat.app.auxclases.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class OswaldRegular extends TextView {

    public OswaldRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OswaldRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OswaldRegular(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.regular.ttf");
            setTypeface(tf);
        }
    }

}