package com.diat.app.auxclases;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.diat.R;

public class AdapterNuestrosProductos extends ArrayAdapter{



    private final Activity context;
    private final String[] title;
    private final String[] description;
    private final Integer[] integers;

    public AdapterNuestrosProductos(Activity context, String[] title, Integer[] integers, String[] description) {
        super(context, R.layout.navigation_list_row_nuestrosproduct, title);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.title=title;
        this.description=description;
        this.integers=integers;
    }

    public View getView(int posicion, View view, ViewGroup parent){

        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.navigation_list_row_nuestrosproduct,null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txTitle);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imgProductos);
        TextView etxDescripcion = (TextView) rowView.findViewById(R.id.txContenido);

        txtTitle.setText(title[posicion]);
        imageView.setImageResource(integers[posicion]);
        etxDescripcion.setText(description[posicion]);

        return rowView;
    }
}
