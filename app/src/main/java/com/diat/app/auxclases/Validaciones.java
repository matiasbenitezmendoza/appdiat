package com.diat.app.auxclases;


import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static android.R.id.list;
import static android.content.Context.MODE_PRIVATE;
import static java.security.AccessController.getContext;


@SuppressWarnings("unused")
public class Validaciones {

    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean isEmail(String email) {
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String fileName(File file) {
        Log.i("File Name", file.getName());
        return file.getName();
    }

    public static byte[] fileArray(File file) {
        return FileByteProcess.getFromAnyFile(file);
    }

    public static String getMimeType(File file) {
        final String[][] fileTypes = {{"jfif", "jfif-tbnl", "jpe", "jpeg", "jpg"}, {"mp3"}};
        final String[] mimeTypes = {"image/jpeg", "audio/mpeg3"};
        try {
            for (int i = 0; i < fileTypes.length; i++)
                for (String type : fileTypes[i])
                    if (file.getName().toLowerCase().endsWith(type))
                        return mimeTypes[i];
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
            if (serviceClass.getName().equals(service.service.getClassName()))
                return true;
        return false;
    }

    public static boolean camposCompletos(ArrayList<EditText> lista) {
        for (EditText e : lista)
            if (e.getText().toString().equals(""))
                return false;
        return true;
    }

    /**
     * Metodo para la respuesta y debug del servicio que la llama.
     *
     * @param label      is(@Nullable) si es null no genere salida en el monitor en caso contrario.
     *                   la cadena enviada es la etiqueta de Debug
     * @param jsonObject objeto Json convertido en cadena para su captura
     * @return devuelve el valor del entero de la respuestas 'success' en el objeto Json de entrada.
     */
    public static int respuestaSencilla(String label, String jsonObject) {
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject)).getAsJsonObject();
            respuestaImpresa(label, object.toString());
            return object.get("success").getAsInt();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Diferencia en el metodo respuestaSencilla en que en vez de recibir success recibe sucess
     *
     * @param label
     * @param jsonObject
     * @return
     */
    public static int respuestaSencillaLogin(String label, String jsonObject) {
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject)).getAsJsonObject();
            respuestaImpresa(label, object.toString());
            return object.get("sucess").getAsInt();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return 0;
        }
    }


    /**
     * Metodo para la respuesta y debug del servicio que la llama.
     *
     * @param label      is(@Nullable) si es null no genere salida en el monitor en caso contrario.
     *                   la cadena enviada es la etiqueta de Debug
     * @param jsonObject objeto Json convertido en cadena para su captura
     * @return devuelve el valor del entero de la respuestas 'success' en el objeto Json de entrada.
     */
    public static int respuestaSencilla(String label, JSONObject jsonObject) {
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());

            return object.get("success").getAsInt();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Metodo para la respuesta y debug del servicio que la llama.
     *
     * @param label      is(@Nullable) si es null no genere salida en el monitor en caso contrario.
     *                   la cadena enviada es la etiqueta de Debug
     * @param jsonObject objeto Json convertido en cadena para su captura
     * @return devuelve el valor como String de la respuestas 'extra' en el objeto Json de entrada.
     */
    public static String respuestaImagen(@Nullable String label, String jsonObject) {
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject)).getAsJsonObject();
            respuestaImpresa(label, object.toString());
            return object.get("imagen").getAsString();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return "Error de la consulta";
        }
    }

    /**
     * Metodo para la respuesta y debug del servicio que la llama.
     *
     * @param label      is(@Nullable) si es null no genere salida en el monitor en caso contrario.
     *                   la cadena enviada es la etiqueta de Debug
     * @param jsonObject objeto Json convertido en cadena para su captura
     * @return devuelve el valor como String de la respuestas 'extra' en el objeto Json de entrada.
     */
    public static String respuestaExtra(@Nullable String label, String jsonObject) {
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject)).getAsJsonObject();
            respuestaImpresa(label, object.toString());
            return object.get("extra").getAsString();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return "Error de la consulta";
        }
    }

    public static ModelChat respuestaChat(String jsonObject) {
        ModelChat modelChat = new ModelChat();
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject).getAsJsonObject());
            try {
                JsonArray array = object.get("mensajes").getAsJsonArray();
                for (JsonElement jsonElement : array) {
                    int id = jsonElement.getAsJsonObject().get("id").getAsInt();
                    int idusuario = jsonElement.getAsJsonObject().get("idusuario").getAsInt();
                    String mensaje = jsonElement.getAsJsonObject().get("mensaje").getAsString();
                    String fecha;
                    try {
                        fecha = jsonElement.getAsJsonObject().get("fecha").getAsString()
                        + "     " + jsonElement.getAsJsonObject().get("hora").getAsString();
                    } catch (UnsupportedOperationException e){
                        e.printStackTrace();
                        fecha = "";
                    }
                    modelChat.setChats(
                            new ModelChat.Chat(id, idusuario, mensaje, fecha)
                    );
                }
            } catch (IllegalStateException e){
                e.printStackTrace();
                modelChat.setChats(new ModelChat.Chat(-1, -1, "Puedes ponerte en contacto con nosotros...", ""));
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return modelChat;
    }

    /**
     * OBTIENE LA RESPUESTA DE REGISTRO
     *
     * @param label
     * @param jsonObject
     * @return el id de usuario y su plan
     */
    public static List<String> respuestaRegistro(String label, String jsonObject) {
        String[] datos = {};
        String succ = "";
        String idC = "";
        String plan = "";
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());

            //CUANDO ES USUARIO REPETIDO SOLO PIDE EL SUCCESS YA QUE LO DEMAS NO SE RECUPERA
            if (object.get("success").getAsString().equals("2")) {
                succ = object.get("success").getAsString();
            } else if (object.get("success").getAsString().equals("1")) {
                succ = object.get("success").getAsString();
                idC = object.get("idcat").getAsString();
                plan = object.get("plan").getAsString();
            }

            return Arrays.asList(
                    succ,
                    idC,
                    plan);


        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * OBTIENE LA RESPUESTA DE REGISTRO DE FORMA DE PAGO, ÚNICAMENTE SE UTILIZA EL PARAMETRO REFERENCIA
     * PARA MOSTRARSELO AL USUARIO
     *
     * @param label
     * @param jsonObject
     * @return
     */
    public static List<String> respuestaPay(String label, String jsonObject) {
        String[] datos = {};
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());

            JSONObject obj = new JSONObject(jsonObject);
            JSONObject result = obj.getJSONObject("cargo");
            String ref = result.getString("reference");

            return Arrays.asList(
                    object.get("success").getAsString(),
                    object.get("error").getAsString(),
                    ref
            );
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * RETORNA INFORMACION DEL USER AL HACER LOGIN
     *
     * @param label
     * @param jsonObject
     * @return
     */
    public static List<String> respuestaLogin(String label, String jsonObject) {
        String[] datos = {};
        String idcat = "";
        String nombre = "";
        String apellido = "";
        String imagen = "";
        String plan = "";
        String notificacion = "";
        String cargo = "", activo = "0", idCompania = "", cuestionario="";


        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());


            if (object.get("sucess").getAsString().equals("1")) {
                JSONObject obj = new JSONObject(jsonObject);
                JSONArray result = obj.getJSONArray("user");
                idcat = result.getJSONObject(0).getString("idcat");
                nombre = result.getJSONObject(0).getString("nombre");
                apellido = result.getJSONObject(0).getString("apellido");
                imagen = result.getJSONObject(0).getString("imagen");

                //uncoment the next line when save te status user
                //String activo = result.getJSONObject(1).getString("activo");

                notificacion = result.getJSONObject(0).getString("notificaciones");
                cargo = result.getJSONObject(0).getString("cobro");
                plan = result.getJSONObject(0).getString("plan");
                idCompania = result.getJSONObject(0).getString("idCompania");
                cuestionario = result.getJSONObject(0).getString("cuestionario");
                if(obj.length() == 3){
                activo = object.get("activo").getAsString();}

            }


            return Arrays.asList(
                    object.get("sucess").getAsString(),
                    idcat,
                    nombre,
                    apellido,
                    imagen,
                    notificacion,
                    cargo,
                    activo,
                    plan,
                    idCompania,
                    cuestionario
            );
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * PARSING THE RESPONSE TO USE WITH PARAMETERS
     *
     * @param label
     * @param jsonObject
     * @return
     */
    public static List<String> responseStart(String label, String jsonObject) {
        String[] datos = {};
        String dieta = "";
        String banner = "";
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());


            /*JSONObject obj = new JSONObject(jsonObject);

            if(object.get("dieta").getAsString()!= ""){
            JSONObject result = obj.getJSONObject("user_peso");
            String pesoActual = result.getString("pesoActual");}*/
            try {
            JSONObject obj = new JSONObject(jsonObject);

                dieta = obj.getString("dieta");
                banner = obj.getString("banner");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return Arrays.asList(
                    object.get("success").getAsString(),
                    dieta,
                    banner
            );
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Metodo para la respuesta y debug del servicio que la llama.
     *
     * @param label      is(@Nullable) si es null no genere salida en el monitor en caso contrario.
     *                   la cadena enviada es la etiqueta de Debug
     * @param jsonObject objeto Json convertido en cadena para su captura
     * @return devuelve arreglo de cadenas con la información de facebook
     */
    public static List<String> respuestaFacebook(String label, JSONObject jsonObject) {
        String[] datos = {};
        try {
            JsonObject object = ((new JsonParser()).parse(jsonObject.toString())).getAsJsonObject();
            respuestaImpresa(label, object.toString());
            return Arrays.asList(
                    object.get("id").getAsString(),
                    object.get("first_name").getAsString(),
                    object.get("last_name").getAsString(),
                    object.get("email").getAsString(),
                    object.getAsJsonObject("picture").getAsJsonObject("data").get("url").getAsString()
            );
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }


    private static void respuestaImpresa(String label, String message) {
        if (label != null)
            Log.e(label, message);
    }

    public static String SHA1(String input) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("SHA1");
            byte[] result = mDigest.digest(input.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : result) {//int i = 0; i < result.length; i++) {
                sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String cleanNull(String input) {
        try {
            return input;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "";
        }
    }
}