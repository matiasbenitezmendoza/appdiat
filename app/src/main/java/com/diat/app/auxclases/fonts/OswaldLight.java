package com.diat.app.auxclases.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class OswaldLight extends TextView {

    public OswaldLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OswaldLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OswaldLight(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.light.ttf");
            setTypeface(tf);
        }
    }

}