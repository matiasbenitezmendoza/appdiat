package com.diat.app.auxclases;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by webandando on 28/11/16.
 */

public class ModelChat {

    private ArrayList<Chat> chats;

    public  ModelChat() {
        chats = new ArrayList<>();
    }

    public ArrayList<Chat> getChats() {
        Collections.reverse(chats);
        return chats;
    }

    public void setChats(Chat chat) {
        chats.add(chat);
    }

    public static class Chat {
        private int id;
        private int idusuario;
        private String mensaje;
        private String fecha;

        public Chat(int id, int idusuario, String mensaje, String fecha) {
            this.id = id;
            this.idusuario = idusuario;
            this.mensaje = mensaje;
            this.fecha = fecha;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIdusuario() {
            return idusuario;
        }

        public void setIdusuario(int idusuario) {
            this.idusuario = idusuario;
        }

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        public String getFecha() {
            return fecha;
        }

        public void setFecha(String fecha) {
            this.fecha = fecha;
        }

        @Override
        public String toString() {
            return "Chat{" +
                    "id=" + id +
                    ", idusuario=" + idusuario +
                    ", mensaje='" + mensaje + '\'' +
                    ", fecha='" + fecha + '\'' +
                    '}';
        }
    }
}
