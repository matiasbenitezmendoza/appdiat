package com.diat.app.auxclases;

@SuppressWarnings("unused")
public class Constantes {

    public static final int PHOTO_CODE = 100;
    public static final int SELECTED_PICTURE = 200;
    public static final int RESULT_CROP = 400;

    public static class WebServices {
        public static String tag_login = "TAG_LOGIN";
        private static String url = "http://webandando.com/proyectos/diat/dadmin/app/";
        //private static String url = "http://codeandote.com/wa/diat/dadmin/app/";
        public static String login = url + "login";
        public static String registro = url + "addUser";
        public static String cuestionario = url + "addUQuestionary";
        public static String registreCard = url + "pay";
        public static String tips = url + "tips";
        public static String info_inicio = url + "info_inicio";
        public static String registra_peso = url + "registra_peso";
        public static String cuestionarioS = url + "addSQuestionary";
        public static String chart = url + "chart";
        public static String dieta_info = url + "dieta_info";
        public static String info_catalogo = url + "info_catalogo";
        public static String menu = url + "menu";
        public static String receta = url + "receta";
        public static String subirImagen = url + "addImage";
        public static String citas = url + "citas";
        public static String agendar = url + "agendar";
        public static String chat = url + "chat";
        public static String logout = url + "logout";

        public static String notifications = url + "notificaciones";


    }

    public static class UserKeys {
        //public static String shared_conf = "conf_values";
        public static String email = "email";
        public static String passwd = "passwd";
        public static String nombre = "nombre";
        public static String apellido = "apellido";
        public static String regimen = "regimen";
        public static String plan = "plan";
        public static String registro = "registro";
    }


    public static class CuestionarioKeys {
        public static String cuestionario = "cuestionario";
        public static String usuario = "usuario";
        public static String identificador = "identificador";

    }


    public static class PayKeys {
        public static String idusuario = "idusuario";
        public static String monto = "monto";
        public static String mensaje = "mensaje";
        public static String token = "token";
        public static String descuento = "descuento";

    }

    public static class LoginKeys {
        public static String usuario = "usuario";
        public static String password = "password";
        public static String token = "token";
    }


    public static class RegisterWeighKeys {
        public static String idusuario = "idusuario";
        public static String peso = "peso";
    }


    public static class UrlImgs {
        //public static String urlImg = "http://codeandote.com/wa/diat/profile/";
        public static String urlImg = "http://webandando.com/proyectos/diat/profile/";

        //public static String urlImgRecetas = "http://codeandote.com/wa/diat/imgs/recetas/";
        public static String urlImgRecetas = "http://webandando.com/proyectos/diat/imgs/recetas/";

        //public static String urlBanner = "http://codeandote.com/wa/diat/imgs/banners/";
        public static String urlBanner = "http://webandando.com/proyectos/diat/imgs/banners/";


    }

    public static class KeysConfig{
        public static String CONFIG = "CONFIGURACION";
        public static String IDCAT = "idcat";
        public static String NOTIFICATIONS = "notificaciones";
        public static String CHARGE = "cobro";

    }
}