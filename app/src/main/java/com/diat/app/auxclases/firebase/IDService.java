package com.diat.app.auxclases.firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class IDService extends FirebaseInstanceIdService {

    private static final String TAG = IDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("Token", refreshedToken);

        storeRegIDInPreference(refreshedToken);

        sendRegistrationToServer(refreshedToken);

        Intent registrationComplete = new Intent(FirebaseKeys.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIDInPreference(String token) {
        getSharedPreferences(FirebaseKeys.SHARED_PREF, MODE_PRIVATE)
                .edit().putString(FirebaseKeys.TOKEN, token).putInt("tipo", -1).apply();
    }
}