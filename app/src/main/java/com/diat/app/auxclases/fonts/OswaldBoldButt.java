package com.diat.app.auxclases.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

public class OswaldBoldButt extends Button {

    public OswaldBoldButt(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OswaldBoldButt(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OswaldBoldButt(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.bold.ttf");
            setTypeface(tf);
        }
    }

}