package com.diat.app.auxclases;

/**
 * Created by WA 05/12/16.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.diat.app.diat.R;


public class AdapterLVRecomendaciones extends BaseAdapter{
    Context context;
    String[] titulos;
    String[] textos;
    LayoutInflater inflater;

    public AdapterLVRecomendaciones(Context context, String[] titulos, String[] textos) {
        this.context = context;
        this.titulos = titulos;
        this.textos = textos;
    }

    @Override
    public int getCount() {
        return titulos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtTexto;  TextView tvTitulo;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_group, parent, false);


        txtTexto = (TextView) itemView.findViewById(R.id.tx_Row_recomendaciones);
        tvTitulo = (TextView) itemView.findViewById(R.id.lblListHeader);

        txtTexto.setText(textos[position]);
        tvTitulo.setText(titulos[position]);

        return itemView;
    }

}