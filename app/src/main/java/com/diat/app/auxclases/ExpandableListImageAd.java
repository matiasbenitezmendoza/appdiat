package com.diat.app.auxclases;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.diat.app.diat.R;

import com.diat.app.webservices.AppController;

import static com.diat.app.auxclases.Constantes.UrlImgs.urlImgRecetas;

public class ExpandableListImageAd extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
    private HashMap<String, List<String>> _listDataChildImages;
    private HashMap<String, List<String>> _listDataChildIds;
    private ImageView imgItems;
    private ImageLoader imageLoader;

    public ExpandableListImageAd(Context context, List<String> listDataHeader, HashMap<String, List<String>> listChildData, HashMap<String, List<String>> listChildDataImages , HashMap<String, List<String>> listChildDataIds) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listDataChildImages = listChildDataImages;
        this._listDataChildIds = listChildDataIds;
        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }


    public Object getChildImages(int groupPosition, int childPosititon) {
        return this._listDataChildImages.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    public Object getChildIds(int groupPosition, int childPosititon) {
        return this._listDataChildIds.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }



    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childTextId = (String) getChildIds(groupPosition, childPosition);
        final String childText = (String) getChild(groupPosition, childPosition);
        final String childTextImage = (String) getChildImages(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_image, null);
        }

        TextView txtIdReceta = (TextView) convertView.findViewById(R.id.tx_idreceta);
        txtIdReceta.setText(childTextId);

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        txtListChild.setText(childText);

        imgItems = (ImageView) convertView.findViewById(R.id.imgItems);
        String url = urlImgRecetas + childTextImage;
        imageLoader.get(url, ImageLoader.getImageListener(imgItems, R.drawable.ico_menu, R.drawable.ico_menu));



        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_image, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeaderImage);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}