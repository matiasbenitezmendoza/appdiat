package com.diat.app.auxclases.firebase;

public class FirebaseKeys {
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "promos";

    public static final String TOKEN = "token";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final int NOTIFICATION_ID_ALERTA = 102;
    public static final int NOTIFICATION_ID_PROMOCION = 103;
    public static final int NOTIFICATION_ID_CHOFER = 104;

    public static final String SHARED_PREF = "ah_firebase";
}