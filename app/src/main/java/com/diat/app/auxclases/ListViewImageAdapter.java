package com.diat.app.auxclases;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diat.app.diat.R;

public class ListViewImageAdapter extends BaseAdapter {
    // Declare Variables
    Context context;
    String[] headers;
    String[] titulos;
    int[] imagenes;
    LayoutInflater inflater;

    public ListViewImageAdapter(Context context,String[] headers, String[] titulos, int[] imagenes) {
        this.context = context;
        this.headers = headers;
        this.titulos = titulos;
        this.imagenes = imagenes;
    }

    @Override
    public int getCount() {
        return titulos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Declare Variables
        TextView txtTitle;
        TextView txTit;
        ImageView imgImg;

        //http://developer.android.com/intl/es/reference/android/view/LayoutInflater.html
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_row_menudieta, parent, false);

        // Locate the TextViews in listview_item.xml
        txTit = (TextView) itemView.findViewById(R.id.textTituloMenu);
        txtTitle = (TextView) itemView.findViewById(R.id.list_row_title);
        imgImg = (ImageView) itemView.findViewById(R.id.list_row_image);

        // Capture position and set to the TextViews
        txTit.setText(headers[position]);
        txtTitle.setText(titulos[position]);
        imgImg.setImageResource(imagenes[position]);

        return itemView;
    }
}