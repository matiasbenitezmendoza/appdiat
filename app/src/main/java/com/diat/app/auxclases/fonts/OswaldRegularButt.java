package com.diat.app.auxclases.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;


public class OswaldRegularButt extends Button {

    public OswaldRegularButt(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OswaldRegularButt(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OswaldRegularButt(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/oswald.regular.ttf");
            setTypeface(tf);
        }
    }

}